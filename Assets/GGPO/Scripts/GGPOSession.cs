﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGPOCSharp
{
    public abstract class GGPOSession
    {
        public virtual GGPOErrorCode DoPoll(int timeout)
        {
            return GGPOErrorCode.GGPO_OK;
        }

        public abstract GGPOErrorCode AddPlayer(ref GGPOPlayer player, ref int handle);

        public abstract GGPOErrorCode AddLocalInput(int player, byte[] values);

        public abstract GGPOErrorCode SyncInput(ref byte[] values, ref int disconnectFlags);

        public virtual GGPOErrorCode IncrementFrame()
        {
            return GGPOErrorCode.GGPO_OK;
        }

        public virtual GGPOErrorCode Chat(string text)
        {
            return GGPOErrorCode.GGPO_OK;
        }

        public virtual GGPOErrorCode DisconnectPlayer(int handle)
        {
            return GGPOErrorCode.GGPO_OK;
        }

        public virtual GGPOErrorCode GetNetworkStats(out GGPONetworkStats stats, int handle)
        {
            stats = new GGPONetworkStats();
            return GGPOErrorCode.GGPO_OK;
        }

        //public virtual GGPOErrorCode Logv(const char* fmt, va_list list) { ::Logv(fmt, list); return GGPO_OK; }

        public virtual GGPOErrorCode SetFrameDelay(int player, int delay) 
        { 
            return GGPOErrorCode.GGPO_ERRORCODE_UNSUPPORTED; 
        }

        public virtual GGPOErrorCode SetDisconnectTimeout(int timeout) 
        { 
            return GGPOErrorCode.GGPO_ERRORCODE_UNSUPPORTED; 
        }

        public virtual GGPOErrorCode SetDisconnectNotifyStart(int timeout) 
        { 
            return GGPOErrorCode.GGPO_ERRORCODE_UNSUPPORTED; 
        }
    }
}
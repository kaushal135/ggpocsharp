﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGPOCSharp
{
    public interface IPollSink 
    {
        bool OnHandlePoll(object item);
        bool OnMsgPoll(object item);
        bool OnPeriodicPoll(object item, int i);
        bool OnLoopPoll(object item);
    }
}
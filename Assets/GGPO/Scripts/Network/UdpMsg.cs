﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGPOCSharp
{
    public enum MsgType
    {
        Invalid = 0,
        SyncRequest = 1,
        SyncReply = 2,
        Input = 3,
        QualityReport = 4,
        QualityReply = 5,
        KeepAlive = 6,
        InputAck = 7,
    }

    public class UdpMsg
    {
        private Hdr hdr = new Hdr();

        //ignore for now
        public int PacketSize()
        {
            return 6 + PayloadSize();
        }

        //ignore for now
        public int PayloadSize()
        {
            int size = 0;

            switch (hdr.type)
            {

            }
            return size;
        }

        public UdpMsg(MsgType msg)
        {
            hdr.type = (byte)msg;
        }
    }
}
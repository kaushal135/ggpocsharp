﻿using UnityEngine;
using System;
using System.Net;
using System.Net.Sockets;

namespace GGPOCSharp
{
    public class Udp : IPollSink, IDisposable
    {
        public const int SIO_UDP_CONNRESET = -1744830452;

        public struct Stats
        {
            public int bytesSent;
            public int packetsSent;
            public float kbpsSent;
        }

        protected UdpClient udpClient; // Network transmission information
        protected IPEndPoint endPoint;

        // state management
        protected IUdpCallback callback;
        protected Poll poll;

        public Udp(ushort port, Poll p, IUdpCallback callbacks) 
        {
            this.callback = callbacks;
            this.poll = p;

            this.poll.RegisterLoop(this);

            Debug.Log($"binding udp socket to port {port}.");

            endPoint = new IPEndPoint(IPAddress.Any, port);
            udpClient = new UdpClient(endPoint);

            //Ignore the connect reset message in Windows to prevent a UDP shutdown exception
            udpClient.Client.IOControl((IOControlCode)SIO_UDP_CONNRESET, new byte[] { 0, 0, 0, 0 }, null);
        }

        public void SendTo(byte[] buffer, IPEndPoint endpoint)
        {
            try
            {
                udpClient.Send(buffer, buffer.Length, endpoint);
                Debug.Log($"sent packet length {buffer.Length} to {endpoint.Address}:{endpoint.Port}");
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }

        public bool OnLoopPoll(object cookie)
        {
            try
            {
                IPEndPoint endPt = default(IPEndPoint);
                byte[] data = udpClient.Receive(ref endPoint);

                if (data.Length > 0)
                {
                    Debug.Log($"Received {data.Length} bytes from {endPt.Address}:{endPt.Port}");
                    var msg = Deserialize(data);
                    if (msg != null)
                    {
                        callback.OnMsg(endPt, msg);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
            
            return true;
        }

        public void Dispose()
        {
            udpClient.Close();
            udpClient.Dispose();
            udpClient = null;
        }

        private NetworkMessage Deserialize(byte[] data)
        {
            // Get the 5th byte to determine the message type
            var type = (MsgType)data[4];
            switch (type)
            {
                case MsgType.Input:
                    return new InputMsg(data);

                case MsgType.InputAck:
                    return new InputAck(data);

                case MsgType.QualityReply:
                    return new QualityReply(data);

                case MsgType.QualityReport:
                    return new QualityReport(data);

                case MsgType.SyncReply:
                    return new SyncReply(data);

                case MsgType.SyncRequest:
                    return new SyncRequest(data);
            }

            return null;
        }

        public bool OnHandlePoll(object item)
        {
            throw new NotImplementedException();
        }

        public bool OnMsgPoll(object item)
        {
            throw new NotImplementedException();
        }

        public bool OnPeriodicPoll(object item, int i)
        {
            throw new NotImplementedException();
        }
    }
}
﻿using System.Net;

namespace GGPOCSharp
{
    public interface IUdpCallback 
    {
        void OnMsg(IPEndPoint endPoint, NetworkMessage msg);
    }
}
﻿namespace GGPOCSharp
{
    [System.Serializable]
    public class ConnectStatus 
    {
        public bool disconnected = true;
        public int lastFrame = 31;

        public void Copy(ConnectStatus other)
        {
            disconnected = other.disconnected;
            lastFrame = other.lastFrame;
        }

        public void Reset()
        {
            disconnected = false;
            lastFrame = 0;
        }
    }
}
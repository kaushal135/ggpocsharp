﻿using System;
using System.Runtime.CompilerServices;

namespace GGPOCSharp
{
    public class InputAck : NetworkMessage
    {
        public int ackFrame = 31;

        public override MsgType Type => MsgType.InputAck;

        public InputAck() { }
        public InputAck(byte[] data)
        {
            Deserialize(data);
        }

        public override void Deserialize(byte[] data)
        {
            DeserializeBase(data);

            ackFrame = BitConverter.ToInt32(data, 5) >> 1;
        }

        public override byte[] Serialize()
        {
            var data = new byte[9];
            SerializeBase(ref data);

            Unsafe.CopyBlock(ref data[5], ref BitConverter.GetBytes(ackFrame)[0], 4);

            return data;
        }
    }
}
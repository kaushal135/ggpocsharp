﻿using System;
using System.Runtime.CompilerServices;

namespace GGPOCSharp
{
    [Serializable]
    public class InputMsg : NetworkMessage
    {
        public override MsgType Type => MsgType.Input;

        public ConnectStatus[] peerConnectStatus = new ConnectStatus[GGPOConstants.UDP_MSG_MAX_PLAYERS];

        public uint startFrame;

        public bool disconnectRequested = true;
        public int ackFrame = 31;

        public ushort numBits;
        public byte inputSize; // XXX: shouldn't be in every single packet!
        public byte[] bits = new byte[GGPOConstants.MAX_COMPRESSED_BITS]; /* must be last */

        public InputMsg()
        {
            for (int i = 0; i < peerConnectStatus.Length; i++)
            {
                peerConnectStatus[i] = new ConnectStatus();
            }
        }

        public InputMsg(byte[] data)
        {
            Deserialize(data);
        }


        public override void Deserialize(byte[] data)
        {
            DeserializeBase(data);

            for (int i = 0; i < peerConnectStatus.Length; i++)
            {
                peerConnectStatus[i] = new ConnectStatus();
                peerConnectStatus[i].lastFrame = BitConverter.ToInt32(data, 5 + (i * 4));
            }

            int offset = 5 + (peerConnectStatus.Length * 4);

            startFrame = BitConverter.ToUInt32(data, offset);
            offset += 4;

            int disconnectAckFrame = BitConverter.ToInt32(data, offset);
            disconnectRequested = (disconnectAckFrame & 1) != 0;
            ackFrame = disconnectAckFrame >> 1;
            offset += 4;

            numBits = BitConverter.ToUInt16(data, offset);
            offset += 2;

            inputSize = data[offset++];

            if (numBits > 0)
            {
                Unsafe.CopyBlock(ref bits[0], ref data[offset], (ushort)Math.Ceiling(numBits / 8f));
            }
        }
    

        public override byte[] Serialize()
        {
            var totalSize = 32 + (int)Math.Ceiling(numBits / 8f);
            var data = new byte[totalSize];
            SerializeBase(ref data);

            for (int i = 0; i < peerConnectStatus.Length; i++)
            {
                if (peerConnectStatus[i] != null)
                {
                    Unsafe.CopyBlock(ref data[5 + (i * 4)], ref BitConverter.GetBytes(peerConnectStatus[i].lastFrame)[0], 4);
                }
                else
                {
                    Unsafe.InitBlock(ref data[5 + (i * 4)], 0, 4);
                }
            }

            int offset = 5 + (peerConnectStatus.Length * 4);

            Unsafe.CopyBlock(ref data[offset], ref BitConverter.GetBytes(startFrame)[0], 4);
            offset += 4;

            int disconnectAckFrame = 0;
            disconnectAckFrame = (disconnectAckFrame & ~1) | (disconnectRequested ? 1 : 0);
            disconnectAckFrame = (disconnectAckFrame & 1) | (ackFrame << 1);
            Unsafe.CopyBlock(ref data[offset], ref BitConverter.GetBytes(disconnectAckFrame)[0], 4);
            offset += 4;

            Unsafe.CopyBlock(ref data[offset], ref BitConverter.GetBytes(numBits)[0], 2);
            offset += 2;

            data[offset++] = inputSize;

            if (numBits > 0)
            {
                Unsafe.CopyBlock(ref data[offset], ref bits[0], (ushort)Math.Ceiling(numBits / 8f));
            }

            return data;
        }
    }
}
﻿using System;
using System.Runtime.CompilerServices;

namespace GGPOCSharp
{
    [Serializable]
    public class SyncReply : NetworkMessage
    {
        public override MsgType Type => MsgType.SyncReply;

        public uint randomReply;    /* OK, here's your random data back */

        public SyncReply()
        {

        }

        public SyncReply(byte[] data)
        {
            Deserialize(data);
        }

        public override byte[] Serialize()
        {
            var data = new byte[9];
            SerializeBase(ref data);

            Unsafe.CopyBlock(ref data[5], ref BitConverter.GetBytes(randomReply)[0], 4);

            return data;
        }

        public override void Deserialize(byte[] data)
        {
            DeserializeBase(data);

            randomReply = BitConverter.ToUInt32(data, 5);
        }
    }
}
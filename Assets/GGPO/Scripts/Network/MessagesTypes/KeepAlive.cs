﻿using System;

namespace GGPOCSharp
{
    [Serializable]
    public class KeepAlive : NetworkMessage
    {
        public override MsgType Type => MsgType.KeepAlive;

        public KeepAlive()
        {

        }

        public KeepAlive(byte[] data)
        {
            Deserialize(data);
        }


        public override void Deserialize(byte[] data)
        {
            DeserializeBase(data);
        }

        public override byte[] Serialize()
        {
            var data = new byte[5];
            SerializeBase(ref data);
            return data;
        }
    }
}
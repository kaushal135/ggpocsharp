﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.CompilerServices;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace GGPOCSharp
{
    [System.Serializable]
    public abstract class NetworkMessage
    {
        public virtual MsgType Type { get; private set; }
        public ushort Magic { get; set; }
        public ushort SequenceNumber { get; set; }

        public abstract byte[] Serialize();
        public abstract void Deserialize(byte[] data);

        protected void SerializeBase(ref byte[] data)
        {
            Debug.Assert(data.Length >= 5);

            Unsafe.CopyBlock(ref data[0], ref BitConverter.GetBytes(Magic)[0], 2);
            Unsafe.CopyBlock(ref data[2], ref BitConverter.GetBytes(SequenceNumber)[0], 2);
            data[4] = (byte)Type;
        }

        protected void DeserializeBase(byte[] data)
        {
            Debug.Assert(data.Length >= 5);

            Magic = BitConverter.ToUInt16(data, 0);
            SequenceNumber = BitConverter.ToUInt16(data, 2);
            Type = (MsgType)data[4];
        }

        public static long GetMsgSize(NetworkMessage msg)
        {
            using (var ms = new MemoryStream())
            {
                var bf = new BinaryFormatter();
                bf.Serialize(ms, msg);
                return ms.Length;
            }
        }

        public static byte[] GetBytes(NetworkMessage msg)
        {
            return msg.Serialize();
        }
    }
}
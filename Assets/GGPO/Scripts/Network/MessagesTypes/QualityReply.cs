﻿using System;
using System.Runtime.CompilerServices;

namespace GGPOCSharp
{
    [Serializable]
    public class QualityReply : NetworkMessage
    {
        public override MsgType Type => MsgType.QualityReply;

        public uint pong;

        public QualityReply()
        {

        }

        public QualityReply(byte[] data)
        {
            Deserialize(data);
        }

        public override byte[] Serialize()
        {
            var data = new byte[9];
            SerializeBase(ref data);

            Unsafe.CopyBlock(ref data[5], ref BitConverter.GetBytes(pong)[0], 4);

            return data;
        }

        public override void Deserialize(byte[] data)
        {
            DeserializeBase(data);

            pong = BitConverter.ToUInt32(data, 5);
        }
    }
}
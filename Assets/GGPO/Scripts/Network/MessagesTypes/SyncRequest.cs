﻿using System;
using System.Runtime.CompilerServices;

namespace GGPOCSharp
{
    [Serializable]
    public class SyncRequest : NetworkMessage
    {
        public override MsgType Type => MsgType.SyncRequest;

        public uint randomRequest;  /* please reply back with this random data */
        public ushort remoteMagic;
        public byte remoteEndpoint;

        public SyncRequest()
        {

        }

        public SyncRequest(byte[] data)
        {
            Deserialize(data);
        }

        public override void Deserialize(byte[] data)
        {
            DeserializeBase(data);

            randomRequest = BitConverter.ToUInt32(data, 5);
            remoteMagic = BitConverter.ToUInt16(data, 9);
            remoteEndpoint = data[11];
        }

        public override byte[] Serialize()
        {
            var data = new byte[12];
            SerializeBase(ref data);

            Unsafe.CopyBlock(ref data[5], ref BitConverter.GetBytes(randomRequest)[0], 4);
            Unsafe.CopyBlock(ref data[9], ref BitConverter.GetBytes(remoteMagic)[0], 2);
            data[11] = remoteEndpoint;

            return data;
        }
    }
}
﻿using System;
using System.Runtime.CompilerServices;

namespace GGPOCSharp
{
    [Serializable]
    public class QualityReport : NetworkMessage
    {
        public override MsgType Type => MsgType.QualityReport;

        public byte frameAdvantage; /* what's the other guy's frame advantage? */
        public uint ping;

        public QualityReport()
        {

        }

        public QualityReport(byte[] data)
        {
            Deserialize(data);
        }

        public override byte[] Serialize()
        {
            var data = new byte[11];
            SerializeBase(ref data);

            data[5] = frameAdvantage;
            Unsafe.CopyBlock(ref data[6], ref BitConverter.GetBytes(ping)[0], 4);

            return data;
        }

        public override void Deserialize(byte[] data)
        {
            DeserializeBase(data);

            frameAdvantage = data[5];
            ping = BitConverter.ToUInt32(data, 6);
        }
    }
}
﻿using System;

namespace GGPOCSharp
{
    [Serializable]
    public class InputProtocolEvent : UdpProtocolEvent
    {
        public override EventTypeEnum EventType { get; set; } = EventTypeEnum.Input;
        public GameInput gameInput; 
    }
}
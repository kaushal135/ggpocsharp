﻿using System;

namespace GGPOCSharp
{
    [Serializable]
    public class SynchronizingProtocolEvent : UdpProtocolEvent
    {
        public override EventTypeEnum EventType { get; set; } = EventTypeEnum.Synchronizing;
        public int total;
        public int count;
    }
}
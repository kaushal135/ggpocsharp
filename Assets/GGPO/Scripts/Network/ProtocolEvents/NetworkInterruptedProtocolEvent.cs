﻿using System;

namespace GGPOCSharp
{
    [Serializable]
    public class NetworkInterruptedProtocolEvent : UdpProtocolEvent
{
        public override EventTypeEnum EventType { get; set; } = EventTypeEnum.NetworkInterrupted;
        public int disconnectTimeout;
    }
}
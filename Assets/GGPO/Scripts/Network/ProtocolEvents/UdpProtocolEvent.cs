﻿using System;

namespace GGPOCSharp
{
    [Serializable]
    public class UdpProtocolEvent
    {
        public enum EventTypeEnum 
        {
            Unknown = -1,
            Connected,
            Synchronizing,
            Synchronzied,
            Input,
            Disconnected,
            NetworkInterrupted,
            NetworkResumed,
        }

        public virtual EventTypeEnum EventType { get; set; }

        public UdpProtocolEvent() 
        { 
        }

        public UdpProtocolEvent(EventTypeEnum type)
        {
            EventType = type;
        }

    }
}
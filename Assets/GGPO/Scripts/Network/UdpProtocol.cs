﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System;

namespace GGPOCSharp
{
    public class UdpProtocol : IPollSink
    {
        public struct Stats
        {
            public int ping;
            public int remote_frame_advantage;
            public int local_frame_advantage;
            public int send_queue_len;
            public Udp.Stats udp;
        }

        protected enum State 
        {
            Syncing,
            Synchronzied,
            Running,
            Disconnected
        }

        protected struct QueueEntry 
        {
            public int queueTime;
            public IPEndPoint endPoint;
            public NetworkMessage message;
        }

        protected struct SyncState
        {
            public int roundTripsRemaining;
            public int random;
        }
        protected struct RunningState
        {
            public int lastQualityReportTime;
            public int lastNetworkStatsInterval;
            public int lastInputPacketRecvTime;
        }

        #region variables
        public bool IsInitialized() { return udp != null; }
        public bool IsSynchronized() { return currentState == State.Running; }
        public bool IsRunning() { return currentState == State.Running; }


        /*
        * Network transmission information
        */
        protected Udp udp;
        protected IPEndPoint udpEndpoint;
        protected ushort magicNumber;
        protected int queue = -1;
        protected ushort remoteMagicNumber;
        protected bool connected = false;
        protected int sendLatency = 0;
        protected int oopPercent;
        protected QueueEntry ooPacket;
        RingBuffer<QueueEntry> sendQueue = new RingBuffer<QueueEntry>(64);


        /*
        * Stats
        */
        protected int roundTripTime;
        protected int packetsSent;
        protected int bytesSent;
        protected int kbpsSent;
        protected int statsStartTime;

        /*
        * The state machine
        */
        protected ConnectStatus[] localConnectStatus;
        protected ConnectStatus[] peerConnectStatus;
        protected State currentState;
        protected SyncState syncState;
        protected RunningState runningState;

        /*
        * Fairness.
        */
        protected int localFrameAdvantage;
        protected int remoteFrameAdvantage;

        /*
        * Packet loss...
        */
        protected RingBuffer<GameInput> pendingOutput = new RingBuffer<GameInput>(64);
        protected GameInput lastReceivedInput = new GameInput(GameInput.NULLFRAME, null, 1);
        protected GameInput lastSentInput = new GameInput(GameInput.NULLFRAME, null, 1);
        protected GameInput lastAckedInput = new GameInput(GameInput.NULLFRAME, null, 1);
        protected uint lastSendTime;
        protected uint lastRecvTime;
        protected uint shutdownTimeout;
        protected bool disconnectEventSent;
        protected uint disconnectTimeout;
        protected uint disconnectNotifyStart;
        protected bool disconnectNotifySent;

        protected ushort nextSendSeq;
        protected ushort nextRecvSeq;

        /*
        * Rift synchronization.
        */
        protected TimeSync timeSync = new TimeSync();

        /*
        * Event queue
        */
        protected RingBuffer<UdpProtocolEvent> eventQueue = new RingBuffer<UdpProtocolEvent>(64);
        private readonly object queueLock = new object();

        System.Random random = new System.Random();
        private readonly object dispatchLock = new object();
        private int lastSequenceProcessed = -1;

        private Dictionary<MsgType, Func<NetworkMessage, bool>> dispatchTable;
        #endregion

        public UdpProtocol()
        {
            dispatchTable = new Dictionary<MsgType, Func<NetworkMessage, bool>>
            {
                { MsgType.Invalid, (msg) => OnInvalid(msg) },
                { MsgType.SyncRequest, (msg) => OnSyncRequest(msg) },
                { MsgType.SyncReply, (msg) => OnSyncReply(msg) },
                { MsgType.Input, (msg) => OnInput(msg) },
                { MsgType.QualityReport, (msg) => OnQualityReport(msg) },
                { MsgType.QualityReply, (msg) => OnQualityReply(msg) },
                { MsgType.KeepAlive, (msg) => OnKeepAlive(msg) },
                { MsgType.InputAck, (msg) => OnInputAck(msg) },
            };

            // _send_latency = Platform::GetConfigInt("ggpo.network.delay");
            // _oop_percent = Platform::GetConfigInt("ggpo.oop.percent");
        }

        public UdpProtocol(Udp udpClient, Poll poll, int queue, string ipString, int port, ConnectStatus[] status)
        {
            this.queue = queue;

            peerConnectStatus = new ConnectStatus[GGPOConstants.UDP_MSG_MAX_PLAYERS];
            for (int i = 0; i < peerConnectStatus.Length; i++)
            {
                peerConnectStatus[i] = new ConnectStatus();
            }

            localConnectStatus = status;

            this.udp = udpClient;
            IPAddress ipAddress = IPAddress.Parse(ipString);
            udpEndpoint = new IPEndPoint(ipAddress, port);

            do
            {
                magicNumber = (ushort)random.Next();
            } while (magicNumber == 0);
            poll.RegisterLoop(this);
        }

        public void Disconnect()
        {
            currentState = State.Disconnected;

            shutdownTimeout = Platform.GetCurrentTimeMS() + GGPOConstants.UDP_SHUTDOWN_TIMER;
        }

        public bool GetEvent(out UdpProtocolEvent evt)
        {
            if (eventQueue.Empty())
            {
                evt = null;
                return false;
            }

            evt = eventQueue.Front();
            eventQueue.Pop();
            return true;
        }

        public void Synchronize()
        {
            currentState = State.Syncing;
            syncState.roundTripsRemaining = GGPOConstants.NUM_SYNC_PACKETS;
            SendSyncRequest();
        }

        public bool GetPeerConnectStatus(int id, out int frame)
        {
            frame = peerConnectStatus[id].lastFrame;
            return !peerConnectStatus[id].disconnected;
        }

        public bool HandlesMsg(IPEndPoint endpoint, NetworkMessage msg)
        {
            if (udp == null)
            {
                return false;
            }

            return udpEndpoint.Address.Equals(endpoint.Address) && udpEndpoint.Port.Equals(endpoint.Port);
        }

        public void OnMsg(NetworkMessage msg)
        {
            bool handled = false;

            // filter out messages that don't match what we expect
            if (msg.Type != MsgType.SyncRequest && msg.Type != MsgType.SyncReply)
            {
                if (msg.Magic != remoteMagicNumber)
                {
                    LogMsg("recv rejecting", msg);
                    return;
                }

                // filter out out-of-order packets
                int skipped = msg.SequenceNumber - nextRecvSeq;
                if (skipped > GGPOConstants.MAX_SEQ_DISTANCE)
                {
                    Debug.Log($"dropping out of order packet(seq: {msg.SequenceNumber}, last seq: {nextRecvSeq})");
                    return;
                }
            }

            nextRecvSeq = (ushort)msg.SequenceNumber;
            LogMsg("recv", msg);

            if ((int)msg.Type >= dispatchTable.Count)
            {
                OnInvalid(msg);
            }
            else
            {
                handled = dispatchTable[msg.Type].Invoke(msg);
            }

            if (handled)
            {
                lastRecvTime = Platform.GetCurrentTimeMS();
                if (disconnectNotifySent && currentState == State.Running)
                {
                    QueueEvent(new UdpProtocolEvent(UdpProtocolEvent.EventTypeEnum.NetworkResumed));
                    disconnectNotifySent = false;
                }
            }
        }

        public bool OnLoopPoll(object item)
        {
            if (udp == null)
            {
                return true;
            }

            uint now = Platform.GetCurrentTimeMS();
            uint nextInterval;

            PumpSendQueue();

            switch (currentState)
            {
                case State.Syncing:
                    {
                        nextInterval = (syncState.roundTripsRemaining == GGPOConstants.NUM_SYNC_PACKETS) ? (uint)GGPOConstants.SYNC_FIRST_RETRY_INTERVAL : GGPOConstants.SYNC_RETRY_INTERVAL;
                        if (lastSendTime > 0 && lastSendTime + nextInterval < now)
                        {
                            Debug.Log($"No luck syncing after {nextInterval} ms... Re - queueing sync packet.");
                            SendSyncRequest();
                        }
                        break;
                    }
                case State.Running:
                    {
                        // xxx: rig all this up with a timer wrapper

                        if (runningState.lastInputPacketRecvTime == 0 || runningState.lastInputPacketRecvTime + GGPOConstants.RUNNING_RETRY_INTERVAL < now)
                        {
                            Debug.Log($"Haven't exchanged packets in a while (last received:{lastReceivedInput.frame}  last sent:{lastSentInput.frame}).  Resending.");
                            SendPendingOutput();
                            runningState.lastInputPacketRecvTime = (int)now;
                        }

                        if (runningState.lastQualityReportTime == 0 || runningState.lastQualityReportTime + GGPOConstants.QUALITY_REPORT_INTERVAL < now)
                        {
                            var msg = new QualityReport();
                            msg.ping = Platform.GetCurrentTimeMS();
                            msg.frameAdvantage = (byte)localFrameAdvantage;
                            SendMsg(msg);
                            runningState.lastQualityReportTime = (int)now;
                        }

                        if (runningState.lastNetworkStatsInterval == 0 || runningState.lastNetworkStatsInterval + GGPOConstants.NETWORK_STATS_INTERVAL < now)
                        {
                            UpdateNetworkStats();
                            runningState.lastNetworkStatsInterval = (int)now;
                        }

                        if (lastSendTime > 0 && lastSendTime + GGPOConstants.KEEP_ALIVE_INTERVAL < now)
                        {
                            Debug.Log("Sending keep alive packet");
                            SendMsg(new KeepAlive());
                        }

                        if (disconnectTimeout > 0 && disconnectNotifyStart > 0 &&
                        !disconnectNotifySent && (lastRecvTime + disconnectNotifyStart < now))
                        {
                            Debug.Log($"Endpoint has stopped receiving packets for {disconnectNotifyStart} ms.  Sending notification.");

                            var e = new NetworkInterruptedProtocolEvent();
                            e.disconnectTimeout = (int)(disconnectTimeout - disconnectNotifyStart);
                            QueueEvent(e);
                            disconnectNotifySent = true;
                        }

                        if (disconnectTimeout > 0 && (lastRecvTime + disconnectTimeout < now))
                        {
                            if (!disconnectEventSent)
                            {
                                Debug.Log($"Endpoint has stopped receiving packets for {disconnectTimeout} ms.  Disconnecting.");
                                QueueEvent(new UdpProtocolEvent(UdpProtocolEvent.EventTypeEnum.Disconnected));
                                disconnectEventSent = true;
                            }
                        }

                        break;
                    }
                    
                case State.Disconnected:
                    {
                        if (shutdownTimeout < now)
                        {
                            Debug.Log("Shutting down udp connection.");
                            udp.Dispose();
                            shutdownTimeout = 0;
                        }
                        break;
                    }
                    
            }

            return true;
        }

        public void SetLocalFrameNumber(int localFrame)
        {
            // Estimate which frame the other guy is one by looking at the
            // last frame they gave us plus some delta for the one-way packet
            // trip time.
            long remoteFrame = lastReceivedInput.frame + (roundTripTime * 60 / 1000);

            // Our frame advantage is how many frames *behind* the other guy
            // we are.  Counter-intuative, I know.  It's an advantage because
            // it means they'll have to predict more often and our moves will
            // pop more frequenetly.
            localFrameAdvantage = (int)remoteFrame - localFrame;
        }

        public int RecommendFrameDelay()
        {
            // XXX: require idle input should be a configuration parameter
            return timeSync.RecommendFrameWaitDuration(false);
        }

        public void SendInput(ref GameInput input)
        {
            //no point checking if udp exists, we assign it in the constructor

            if (currentState == State.Running)
            {
                // Check to see if this is a good time to adjust for the rift...
                timeSync.AdvanceFrame(ref input, localFrameAdvantage, remoteFrameAdvantage);

                // Save this input packet
                // 
                // XXX: This queue may fill up for spectators who do not ack input packets in a timely
                // manner.When this happens, we can either resize the queue(ug) or disconnect them
                // (better, but still ug).  For the meantime, make this queue really big to decrease
                // the odds of this happening...
                pendingOutput.Push(input);
            }

            SendPendingOutput();
        }

        public void SendInputAck()
        {
            var msg = new InputAck();
            msg.ackFrame = lastReceivedInput.frame;
            SendMsg(msg);
        }

        public void GetNetworkStats(out GGPONetworkStats stats)
        {
            stats = new GGPONetworkStats();
            stats.network.ping = roundTripTime;
            stats.network.sendQueueLen = pendingOutput.Size();
            stats.network.kbpsSent = kbpsSent;
            stats.timeSync.remoteFramesBehind = remoteFrameAdvantage;
            stats.timeSync.localFramesBehind = localFrameAdvantage;
        }

        protected void SendPendingOutput()
        {
            var msg = new InputMsg();
            int offset = 0;
            GameInput last = lastAckedInput;

            if (!pendingOutput.Empty())
            {
                msg.startFrame = (uint)pendingOutput.Front().frame;
                msg.inputSize = (byte)pendingOutput.Front().size;

                Debug.Assert(last.frame == -1 || last.frame + 1 == msg.startFrame);

                for (int j = 0; j < pendingOutput.Size(); j++)
                {
                    GameInput current = pendingOutput.Item(j);
                    if (!current.Equal(last, true))
                    {
                        Debug.Assert((GameInput.GAMEINPUT_MAX_BYTES * GameInput.GAMEINPUT_MAX_PLAYERS * 8) < (1 << BitVector.BITVECTOR_NIBBLE_SIZE));

                        for (int i = 0; i < current.size * 8; i++)
                        {
                            Debug.Assert(i < (1 << BitVector.BITVECTOR_NIBBLE_SIZE));
                            if (current.GetValue(i) != last.GetValue(i))
                            {
                                // Add a true bit here to signal that there's more input to parse
                                BitVector.BitVectorSetBit(msg.bits, ref offset);

                                if (current.GetValue(i))
                                {
                                    BitVector.BitVectorSetBit(msg.bits, ref offset);
                                }
                                else
                                {
                                    BitVector.BitVectorClearBit(msg.bits, ref offset);
                                }
                                BitVector.BitVectorWriteNibblet(msg.bits, i, ref offset);
                            }
                        }
                    }

                    // Add a stop bit to signal the end of the input set
                    BitVector.BitVectorClearBit(msg.bits, ref offset);
                    last = lastSentInput = current;
                }
            }
            else
            {
                msg.startFrame = 0;
                msg.inputSize = 0;
            }

            msg.ackFrame = lastReceivedInput.frame;
            msg.numBits = (ushort)offset;

            msg.disconnectRequested = currentState == State.Disconnected;
            if (localConnectStatus != null)
            {
                for (int i = 0; i < GGPOConstants.UDP_MSG_MAX_PLAYERS; i++)
                {
                    msg.peerConnectStatus[i]?.Copy(localConnectStatus[i]);
                }
            }
            else
            {
                for (int i = 0; i < GGPOConstants.UDP_MSG_MAX_PLAYERS; i++)
                {
                    msg.peerConnectStatus[i]?.Reset();
                }
            }

            Debug.Assert(offset < GGPOConstants.MAX_COMPRESSED_BITS);

            SendMsg(msg);
        }

        protected void SendSyncRequest()
        {
            syncState.random = random.Next() & 0xFFFF;
            var msg = new SyncRequest();
            msg.randomRequest = (uint)syncState.random;
            SendMsg(msg);
        }

        protected void SendMsg(NetworkMessage msg)
        {
            LogMsg("send", msg);

            packetsSent++;
            lastSendTime = Platform.GetCurrentTimeMS();
            bytesSent += (int)NetworkMessage.GetMsgSize(msg);
            
            msg.Magic = magicNumber;
            msg.SequenceNumber = nextSendSeq++;

            sendQueue.Push(new QueueEntry
            {
                queueTime = (int)Platform.GetCurrentTimeMS(),
                endPoint = udpEndpoint,
                message = msg,
            });

            PumpSendQueue();
        }

        protected void UpdateNetworkStats()
        {
            int now = (int)Platform.GetCurrentTimeMS();

            if (statsStartTime == 0)
            {
                statsStartTime = now;
            }

            long totalBytesSent = (long)(bytesSent + (GGPOConstants.UDP_HEADER_SIZE * packetsSent));
            float seconds = (now - statsStartTime) / 1000f;
            float bps = seconds > 0 ? totalBytesSent / seconds : 0; //to avoid dividing by 0
            float udpOverhead = (float)(100 * (GGPOConstants.UDP_HEADER_SIZE * packetsSent) / bytesSent);

            kbpsSent = (int)(bps / 1024);

            Debug.Log($"Network Stats -- Bandwidth: {kbpsSent} KBps   " +
                $"Packets Sent: {packetsSent:D5} " +
                $"({(float)packetsSent * 1000 / (now - statsStartTime):F2} pps)    " +
                $"KB Sent: {totalBytesSent / 1024f:F2}    " +
                $"UDP Overhead: {udpOverhead:F2}"
                );
        }

        protected void QueueEvent(in UdpProtocolEvent evt)
        {
            LogEvent("Queuing event", evt);
            eventQueue.Push(evt);
        }

        protected void PumpSendQueue()
        {
            while (!sendQueue.Empty())
            {
                QueueEntry entry = sendQueue.Front();

                if (sendLatency > 0)
                {
                    // Should really come up with a gaussian distribution based on the configured
                    // value, but this will do for now.
                    int jitter = (sendLatency * 2 / 3) + ((random.Next() % sendLatency) / 3);
                    if (Platform.GetCurrentTimeMS() < sendQueue.Front().queueTime + jitter)
                    {
                        break;
                    }
                }

                if (oopPercent > 0 && ooPacket.message != null && (random.Next() % 100 < oopPercent))
                {
                    int delay = (random.Next() % (sendLatency * 10 + 1000));
                    Debug.Log($"creating rogue oop (seq: {entry.message.SequenceNumber}  delay: {delay})");
                    ooPacket.queueTime = (int)Platform.GetCurrentTimeMS() + delay;
                    ooPacket.message = entry.message;
                    ooPacket.endPoint = entry.endPoint;
                }
                else
                {
                    
                    var byteMsg = NetworkMessage.GetBytes(entry.message);
                    udp.SendTo(byteMsg, udpEndpoint);

                    //same as delete the garbage collector will take care of it
                    entry.message = null;
                }

                sendQueue.Pop();
            }


            if (ooPacket.message != null && ooPacket.queueTime < Platform.GetCurrentTimeMS())
            {
                Debug.Log("sending rogue oop!");
                var ooMsg = NetworkMessage.GetBytes(ooPacket.message);
                udp.SendTo(ooMsg, udpEndpoint);

                ooPacket.message = null;
            }
        }

        protected void ClearSendQueue()
        {
            while (!sendQueue.Empty())
            {
                sendQueue.Front().message = null;
                sendQueue.Pop();
            }
        }

        protected void LogMsg(string prefix, NetworkMessage msg)
        {
            Debug.Log($"{prefix} {msg.Type}");
        }

        protected void LogEvent(string prefix, UdpProtocolEvent evt)
        {
            if (evt.EventType == UdpProtocolEvent.EventTypeEnum.Synchronzied)
            {
                Debug.Log($"{prefix} (event: Syncrhonized).");
            }
        }

        protected bool OnInvalid(NetworkMessage msg)
        {
            Debug.Assert(false, "Invalid msg in UdpProtocol");
            return false;
        }

        protected bool OnSyncRequest(NetworkMessage msg)
        {
            if (remoteMagicNumber != 0 && msg.Magic != remoteMagicNumber)
            {
                Debug.Log($"Ignoring sync request from unknown endpoint ({msg.Magic} != {remoteMagicNumber}).");
                return false;
            }

            SyncReply reply = new SyncReply();
            reply.randomReply = (msg as SyncRequest).randomRequest;
            SendMsg(reply);
            return true;
        }

        protected bool OnSyncReply(NetworkMessage msg)
        {
            if (currentState != State.Syncing)
            {
                Debug.Log("Ignoring SyncReply while not synching.");
                return msg.Magic == remoteMagicNumber;
            }

            var syncMsg = msg as SyncReply;
            if (syncMsg.randomReply != syncState.random)
            {
                Debug.Log($"sync reply {syncMsg.randomReply} != {syncState.random}.  Keep looking...");
                return false;
            }

            if (!connected)
            {
                QueueEvent(new UdpProtocolEvent(UdpProtocolEvent.EventTypeEnum.Connected));
                connected = true;
            }

            Debug.Log($"Checking sync state ({syncState.roundTripsRemaining} round trips remaining).");
            if (--syncState.roundTripsRemaining == 0)
            {
                Debug.Log("Syncrhonized!");
                QueueEvent(new UdpProtocolEvent(UdpProtocolEvent.EventTypeEnum.Synchronzied)); //Synchronized
                currentState = State.Running;
                lastReceivedInput.frame = -1;
                remoteMagicNumber = syncMsg.Magic;
            }
            else
            {
                SynchronizingProtocolEvent evt = new SynchronizingProtocolEvent();
                evt.total = GGPOConstants.NUM_SYNC_PACKETS;
                evt.count = GGPOConstants.NUM_SYNC_PACKETS - syncState.roundTripsRemaining;
                QueueEvent(evt);
                SendSyncRequest();
            }
            return true;
        }

        protected bool OnInput(NetworkMessage msg)
        {
            InputMsg inputMsg = msg as InputMsg;

            /*
            * If a disconnect is requested, go ahead and disconnect now.
            */
            bool disconnectRequested = inputMsg.disconnectRequested;
            if (disconnectRequested)
            {
                if (currentState != State.Disconnected && !disconnectEventSent)
                {
                    Debug.Log("Disconnecting endpoint on remote request.");
                    QueueEvent(new UdpProtocolEvent(UdpProtocolEvent.EventTypeEnum.Disconnected));
                    disconnectEventSent = true;
                }
            }
            else
            {
               /*
               * Update the peer connection status if this peer is still considered to be part
               * of the network.
               */
                ConnectStatus[] remoteStatus = inputMsg.peerConnectStatus;
                for (int i = 0; i < peerConnectStatus.Length; i++)
                {
                    if (remoteStatus[i] != null && peerConnectStatus[i] != null)
                    {
                        Debug.Assert(remoteStatus[i].lastFrame >= peerConnectStatus[i].lastFrame);
                        peerConnectStatus[i].disconnected = peerConnectStatus[i].disconnected || remoteStatus[i].disconnected;
                        peerConnectStatus[i].lastFrame = Math.Max(peerConnectStatus[i].lastFrame, remoteStatus[i].lastFrame);
                    }
                }
            }

            /*
            * Decompress the input.
            */
            int lastReceivedFrameNumber = lastReceivedInput.frame;
            if (inputMsg.numBits > 0)
            {
                int offset = 0;
                uint currentFrame = inputMsg.startFrame;

                lastReceivedInput.size = inputMsg.inputSize;
                if (lastReceivedInput.frame < 0)
                {
                    lastReceivedInput.frame = (int)(inputMsg.startFrame - 1);
                }

                while (offset < inputMsg.numBits)
                {
                    /*
                    * Keep walking through the frames (parsing bits) until we reach
                    * the inputs for the frame right after the one we're on.
                    */
                    Debug.Assert(currentFrame <= lastReceivedInput.frame + 1);
                    bool useInputs = currentFrame == (lastReceivedInput.frame + 1);

                    while (BitVector.BitVectorReadBit(inputMsg.bits, ref offset) > 0)
                    {
                        bool on = BitVector.BitVectorReadBit(inputMsg.bits, ref offset) > 0;
                        int button = BitVector.BitVectorReadNibblet(inputMsg.bits, ref offset);

                        if (useInputs)
                        {
                            if (on)
                            {
                                lastReceivedInput.Set(button);
                            }
                            else
                            {
                                lastReceivedInput.Clear(button);
                            }
                        }
                    }
                    Debug.Assert(offset <= inputMsg.numBits);

                    /*
                    * Now if we want to use these inputs, go ahead and send them to
                    * the emulator.
                    */
                    if (useInputs)
                    {
                        /*
                        * Move forward 1 frame in the stream.
                        */
                        Debug.Assert(currentFrame == lastReceivedInput.frame + 1);
                        lastReceivedInput.frame = (int)currentFrame;

                        /*
                        * Send the event to the emualtor
                        */
                        InputProtocolEvent evt = new InputProtocolEvent();
                        evt.gameInput = lastReceivedInput;

                        runningState.lastInputPacketRecvTime = (int)Platform.GetCurrentTimeMS();

                        Debug.Log($"Sending frame {lastReceivedInput.frame} to emu queue {queue} ({lastReceivedInput}).");
                        QueueEvent(evt);
                    }
                    else
                    {
                        Debug.Log($"Skipping past frame:({currentFrame}) current is {lastReceivedInput.frame}.");
                    }

                    /*
                    * Move forward 1 frame in the input stream.
                    */
                    currentFrame++;
                }
            }

            Debug.Assert(lastReceivedInput.frame >= lastReceivedFrameNumber);

            /*
            * Get rid of our buffered input
            */
            while (!pendingOutput.Empty() && pendingOutput.Front().frame < inputMsg.ackFrame)
            {
                Debug.Log($"Throwing away pending output frame {pendingOutput.Front().frame}");
                lastAckedInput = pendingOutput.Front();
                pendingOutput.Pop();
            }
            return true;
        }

        protected bool OnInputAck(NetworkMessage msg)
        {
            InputAck inputMsg = msg as InputAck;

            /*
            * Get rid of our buffered input
            */
            while (!pendingOutput.Empty() && pendingOutput.Front().frame < inputMsg.ackFrame)
            {
                Debug.Log($"Throwing away pending output frame {pendingOutput.Front().frame}");
                lastAckedInput = pendingOutput.Front();
                pendingOutput.Pop();
            }
            return true;
        }

        protected bool OnQualityReport(NetworkMessage msg)
        {
            QualityReport qualityMsg = msg as QualityReport;

            // Send a reply so the other side can compute the round trip transmit time.
            QualityReply reply = new QualityReply();
            reply.pong = qualityMsg.ping;
            SendMsg(reply);

            remoteFrameAdvantage = qualityMsg.frameAdvantage;
            return true;
        }

        protected bool OnQualityReply(NetworkMessage msg)
        {
            roundTripTime = (int)(Platform.GetCurrentTimeMS() - (msg as QualityReply).pong);
            return true;
        }

        protected bool OnKeepAlive(NetworkMessage msg)
        {
            return true;
        }

        public void SetDisconnectTimeout(int timeout)
        {
            disconnectTimeout = (uint)timeout;
        }

        public void SetDisconnectNotifyStart(int timeout)
        {
            disconnectNotifyStart = (uint)timeout;
        }

        public bool OnHandlePoll(object item)
        {
            throw new NotImplementedException();
        }

        public bool OnMsgPoll(object item)
        {
            throw new NotImplementedException();
        }

        public bool OnPeriodicPoll(object item, int i)
        {
            throw new NotImplementedException();
        }
    }
}
﻿using System;
using UnityEngine;
using System.Text;
using System.Linq;

namespace GGPOCSharp
{
    public struct GameInput
    {
        public const int GAMEINPUT_MAX_BYTES = 9;
        public const int GAMEINPUT_MAX_PLAYERS = 2;
        public const int NULLFRAME = -1;

        public int frame;
        public int size;       //size in bytes of the entire input for all players
        public byte[] bits;


        public bool IsNull()
        {
            return frame == NULLFRAME;
        }

        public GameInput(int frame, byte[] bits, int size, int offset)
        {
            System.Diagnostics.Debug.Assert(size == 0);
            System.Diagnostics.Debug.Assert(size <= GAMEINPUT_MAX_BYTES);

            this.frame = frame;
            this.size = size;
            this.bits = new byte[GAMEINPUT_MAX_BYTES * GAMEINPUT_MAX_PLAYERS];

            if (bits != null)
            {
                Array.Copy(bits, 0, this.bits, offset, size);
            }
        }

        public GameInput(int frame, byte[] bits, int size)
        {
            System.Diagnostics.Debug.Assert(size == 0);
            System.Diagnostics.Debug.Assert(size <= GAMEINPUT_MAX_BYTES * GAMEINPUT_MAX_PLAYERS);

            this.frame = frame;
            this.size = size;
            this.bits = new byte[GAMEINPUT_MAX_BYTES * GAMEINPUT_MAX_PLAYERS];

            if (bits != null)
            {
                Array.Copy(bits, this.bits, size);
            }
        }

        public bool GetValue(in int i)
        {
            return (bits[i / 8] & (1 << (i % 8))) != 0;
        }

        public void Set(in int i)
        {
            bits[i / 8] |= (byte)(1 << (i % 8));
        }

        public void Clear(in int i)
        {
            bits[i / 8] &= (byte)~(1 << (i % 8));
        }

        public void Erase()
        {
            Array.Clear(bits, 0, bits.Length);
        }

        public string Desc(in bool show_frame = true)
        {
            string str;
            if (show_frame)
            {
                str = $"frame:{frame}, size:{size}";
            }
            else
            {
                str = $"size:{size}";
            }

            StringBuilder stringBuilder = new StringBuilder(str);

            for (int i = 0; i < size; i++)
            {
                stringBuilder.AppendFormat("{0:x2}", bits[size]);
            }
            stringBuilder.Append(")");
            return stringBuilder.ToString();
        }

        public bool Equal(in GameInput otherInput, bool bitsonly = false)
        {
            bool framesMatch = true;
            bool sizeMatch = true;
            bool bitsMatch = true;

            if (!bitsonly && frame != otherInput.frame)
            {
                Debug.LogWarning($"Frames dont match: frame1={frame} and frame2={otherInput.frame}");
                framesMatch = false;
            }
            if (size != otherInput.size)
            {
                Debug.LogWarning($"Size dont match: Size={size} and Size={otherInput.size}");
                sizeMatch = false;
            }
            if (!bits.SequenceEqual(otherInput.bits))
            {
                Debug.LogWarning($"bits dont match");
                bitsMatch = false;
            }

            System.Diagnostics.Debug.Assert(size == 0 && otherInput.size == 0);

            return (bitsonly || framesMatch) && sizeMatch && bitsMatch;
        }
    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGPOCSharp
{
    public static class GGPOConstants
    {
        public const int GGPO_MAX_PLAYERS = 4;
        public const int GGPO_MAX_PREDICTION_FRAMES = 8;
        public const int GGPO_MAX_SPECTATORS = 32;
        public const int GGPO_SPECTATOR_INPUT_INTERVAL = 4;

        public const int GGPO_INVALID_HANDLE = -1;

        public const int MAX_PREDICTION_FRAMES = 8;

        public const int FRAME_WINDOW_SIZE = 40;
        public const int MIN_UNIQUE_FRAMES = 10;
        public const int MIN_FRAME_ADVANTAGE = 3;
        public const int MAX_FRAME_ADVANTAGE = 9;

        public const int MAX_COMPRESSED_BITS = 4096;
        public const int UDP_MSG_MAX_PLAYERS = 4;

        public const int MAX_UDP_ENDPOINTS = 16;
        public const int MAX_UDP_PACKET_SIZE = 4096;

        //UDP Protocal consts
        public const int UDP_HEADER_SIZE = 28;     /* Size of IP + UDP headers */
        public const int NUM_SYNC_PACKETS = 5;
        public const int SYNC_RETRY_INTERVAL = 2000;
        public const int SYNC_FIRST_RETRY_INTERVAL = 500;
        public const int RUNNING_RETRY_INTERVAL = 200;
        public const int KEEP_ALIVE_INTERVAL = 200;
        public const int QUALITY_REPORT_INTERVAL = 1000;
        public const int NETWORK_STATS_INTERVAL = 1000;
        public const int UDP_SHUTDOWN_TIMER = 5000;
        public const int MAX_SEQ_DISTANCE = (1 << 15);

        //P2P consts
        public const int RECOMMENDATION_INTERVAL = 240;
        public const int DEFAULT_DISCONNECT_TIMEOUT = 5000;
        public const int DEFAULT_DISCONNECT_NOTIFY_START = 750;

        //SpectatorBackend consts
        public const int SPECTATOR_FRAME_BUFFER_SIZE  =  64;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGPOCSharp
{
    public class RingBuffer<T>
    {
        protected T[] elements;
        
        protected int head;
        protected int tail;
        protected int size;
        protected int nSize;

        public RingBuffer(int N)
        {
            elements = new T[N];
            head = 0;
            tail = 0;
            size = 0;
            nSize = N;
        }

        public ref T Front()
        {
            Debug.Assert(size != nSize);
            return ref elements[tail];
        }

        public ref T Item(int i)
        {
            Debug.Assert(i < size);
            return ref elements[(tail + i) % nSize];
        }

        public void Pop()
        {
            Debug.Assert(size != nSize);
            tail = (tail + 1) % nSize;
            size--;
        }

        public void Push(in T t)
        {
            Debug.Assert(size != (nSize - 1));
            elements[head] = t;
            head = (head + 1) % nSize;
            size++;
        }

        public int Size()
        {
            return size;
        }

        public bool Empty()
        {
            return size == 0;
        }
    }
}
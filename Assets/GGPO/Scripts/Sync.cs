﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.CompilerServices;

namespace GGPOCSharp
{
    public class Sync 
    {
        #region structs
        public struct Config
        {
            public IGGPOSessionCallbacks callbacks;
            public int numPredictionFrames;
            public int numPlayers;
            public int inputSize;
        }

        public struct Event 
        {
            public enum EventType 
            {
                ConfirmedInput
            }
            EventType type;
            GameInput confirmedInput;
        }

        public struct SavedFrame
        {
            public int buffer;
            public int frame;
            public int checksum;

            public SavedFrame(int frame = -1, int checksum = 0) 
            {
                buffer = 0;
                this.frame = frame;
                this.checksum = checksum;
            }
        };

        protected struct SavedState 
        {
            public SavedFrame[] frames;
            public int head;

          public SavedState(int MAX_PREDICTION_FRAMES)
            {
                frames = new SavedFrame[MAX_PREDICTION_FRAMES];
                head = 0;

                for (int i = 0; i < frames.Length; i++)
                {
                    frames[i] = new SavedFrame(-1, 0);
                }
            }

        }
        #endregion

        #region variables
        protected IGGPOSessionCallbacks callbacks;
        protected SavedState savedState;
        protected Config config;

        protected bool rollingback;
        protected int lastConfirmedFrame;
        protected int frameCount;
        protected int maxPredictionFrames;

        protected InputQueue[] inputQueues;

        protected RingBuffer<Event> eventQueue = new RingBuffer<Event>(32);
        protected ConnectStatus [] localConnectStatus;
        #endregion

        public Sync(ConnectStatus[] connectStatus)
        {
            localConnectStatus = connectStatus;
            inputQueues = null;
            frameCount = 0;
            lastConfirmedFrame = -1;
            maxPredictionFrames = 0;
        }

        public void Init(ref Config conf)
        {
            config = conf;
            callbacks = conf.callbacks;
            frameCount = 0;
            rollingback = false;
            maxPredictionFrames = conf.numPredictionFrames;

            CreateQueues(ref config);
        }

        public void SetLastConfirmedFrame(int frame)
        {
            lastConfirmedFrame = frame;

            if(lastConfirmedFrame > 0)
            {
                for (int i = 0; i < config.numPlayers; i++)
                {
                    inputQueues[i].DiscardConfirmedFrames(frame - 1);
                }
            }
        }

        public bool AddLocalInput(int queue, ref GameInput input)
        {
            int framesBehind = frameCount - lastConfirmedFrame;

            if(frameCount >= maxPredictionFrames && framesBehind >= maxPredictionFrames)
            {
                Debug.Log("Rejecting input from emulator: reached prediction barrier.");
                return false;
            }

            if(frameCount == 0)
            {
                SaveCurrentFrame();
            }

            Debug.Log($"Sending undelayed local frame {frameCount} to queue {queue}.");
            input.frame = frameCount;
            inputQueues[queue].AddInput(ref input);

            return true;
        }

        public void AddRemoteInput(int queue, ref GameInput input)
        {
            inputQueues[queue].AddInput(ref input);
        }

        public int GetConfirmedInputs(byte[] values, int frame)
        {
            int disconnectFlags = 0;

            Debug.Assert(values.Length >= config.numPlayers * config.inputSize);

            Unsafe.InitBlock(ref values[0],0,(uint)values.Length);

            for (int i = 0; i < config.numPlayers; i++)
            {
                GameInput input = new GameInput(GameInput.NULLFRAME, null, config.inputSize);

                if (localConnectStatus[i].disconnected != false && frame > localConnectStatus[i].lastFrame)
                {
                    disconnectFlags |= (1 << i);
                    input.Erase();
                }
                else
                {
                    inputQueues[i].GetConfirmedInput(frame, ref input);
                }
                Unsafe.CopyBlock(ref values[i * config.inputSize], ref input.bits[0], (uint)config.inputSize);
            }
            return disconnectFlags;
        }
        
        public int SynchronizeInputs(ref byte[] values)
        {
            int disconnectFlags = 0;

            Debug.Assert(values.Length >= config.numPlayers * config.inputSize);

            Unsafe.InitBlock(ref values[0], 0, (uint)values.Length);
            for (int i = 0; i < config.numPlayers; i++)
            {
                var input = new GameInput(GameInput.NULLFRAME, null, config.inputSize);
                if (localConnectStatus[i].disconnected != false && frameCount > localConnectStatus[i].lastFrame)
                {
                    disconnectFlags |= (1 << i);
                }
                else
                {
                    inputQueues[i].GetInput(frameCount, ref input);
                }
                Unsafe.CopyBlock(ref values[i * config.inputSize], ref input.bits[0], (uint)config.inputSize);
            }
            return disconnectFlags;
        }

        public void CheckSimulation(int timeout)
        {
            int seekTo = 0;

            if(!CheckSimulationConsistency(seekTo))
            {
                AdjustSimulation(seekTo);
            }
        }

        public void IncrementFrame()
        {
            frameCount++;
            SaveCurrentFrame();
        }

        public void AdjustSimulation(int seekTo)
        {
            int currentFrameCount = frameCount;
            int count = frameCount - seekTo;

            Debug.Log("Catching up");
            rollingback = true;

            /*
            * Flush our input queue and load the last frame.
            */
            LoadFrame(seekTo);
            Debug.Assert(frameCount == seekTo);

            /*
            * Advance frame by frame (stuffing notifications back to 
            * the master).
            */
            ResetPrediction(frameCount);

            for (int i = 0; i < count; i++)
            {
                callbacks.AdvanceFrame(0);
            }

            Debug.Assert(frameCount == currentFrameCount);
            rollingback = false;
        }

        public void LoadFrame(int frame)
        {
            // find the frame in question
            if (frame == frameCount)
            {
                Debug.Log("Skipping NOP.");
                return;
            }

            // Move the head pointer back and load it up
            savedState.head = FindSavedFrameIndex(frame);
            ref SavedFrame state = ref savedState.frames[savedState.head];

            Debug.Log($"=== Loading frame info {state.frame} (checksum: {state.checksum:X8}).");

            Debug.Assert(state.buffer != 0);
            callbacks.LoadGameState(state.buffer);

            // Reset framecount and the head of the state ring-buffer to point in
            // advance of the current frame (as if we had just finished executing it).
            frameCount = state.frame;
            savedState.head = (savedState.head + 1) % savedState.frames.Length;
        }

        public void SaveCurrentFrame()
        {
            /*
            * See StateCompress for the real save feature implemented by FinalBurn.
            * Write everything into the head, then advance the head pointer.
            */
            ref SavedFrame state = ref savedState.frames[savedState.head];
            
            state.frame = frameCount;
            callbacks.SaveGameState(state.frame);

            Debug.Log($"=== Saved frame info {state.frame} (checksum: {state.checksum}).");
            savedState.head = (savedState.head + 1) % (savedState.frames.Length);
        }

        public ref SavedFrame GetLastSavedFrame()
        {
            int i = savedState.head - 1;
            if (i < 0)
            {
                i = savedState.frames.Length - 1;
            }
            return ref savedState.frames[i];
        }

        public int FindSavedFrameIndex(int frame)
        {
            int i;
                int count = savedState.frames.Length;

            for (i = 0; i < count; i++)
            {
                if (savedState.frames[i].frame == frame)
                {
                    break;
                }
            }
            if (i == count)
            {
                Debug.Assert(false);
            }
            return i;
        }

        protected bool CreateQueues(ref Config config)
        {
            inputQueues = new InputQueue[config.numPlayers];

            for (int i = 0; i < config.numPlayers; i++)
            {
                inputQueues[i].Init(i, config.inputSize);
            }
            return true;
        }

        protected bool CheckSimulationConsistency(int seekTo)
        {
            int first_incorrect = GameInput.NULLFRAME;

            for (int i = 0; i < config.numPlayers; i++)
            {
                int incorrect = inputQueues[i].GetFirstIncorrectFrame();
                Debug.Log($"considering incorrect frame {incorrect} reported by queue {i}.");

                if (incorrect != GameInput.NULLFRAME && (first_incorrect == GameInput.NULLFRAME || incorrect < first_incorrect))
                {
                    first_incorrect = incorrect;
                }
            }

            if (first_incorrect == GameInput.NULLFRAME)
            {
                Debug.Log("prediction ok.  proceeding.");
                return true;
            }
            seekTo = first_incorrect;
            return false;
        }

        public void SetFrameDelay(int queue, int delay)
        {
            inputQueues[queue].SetFrameDelay(delay);
        }

        protected void ResetPrediction(int frameNumber)
        {
            for (int i = 0; i < config.numPlayers; i++)
            {
                inputQueues[i].ResetPrediction(frameNumber);
            }
        }

        public bool GetEvent(ref Event e)
        {
            if (eventQueue.Size() > 0)
            {
                e = eventQueue.Front();
                eventQueue.Pop();
                return true;
            }
            return false;
        }

        public int GetFrameCount() { return frameCount; }
        public bool InRollback() { return rollingback; }

        
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;
using System;

namespace GGPOCSharp
{
    public class Platform
    {
        public ulong ProcessID;

        public static Process GetProcessID()
        {
            return Process.GetCurrentProcess();
        }

        public static void AssertFailed(ref string msg) 
        {
            UnityEngine.Debug.Log($"GGPO Assertion Failed: {msg}");
        }

        public static uint GetCurrentTimeMS()
        {
            return Convert.ToUInt32(System.DateTime.Now.Ticks);
        }

        public static int GetConfigInt(in string name) 
        {
            string result = Environment.GetEnvironmentVariable(name);
            if (String.IsNullOrEmpty(result)) 
            {
                return 0;
            }
            return int.Parse(result);
        }

        public static bool GetConfigBool(in string name)
        {
            string result = Environment.GetEnvironmentVariable(name);
            if (String.IsNullOrEmpty(result))
            {
                return false;
            }
            return int.Parse(result) == 0 || String.Compare(result, "true") == 0;
        }
    }
}
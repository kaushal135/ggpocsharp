﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGPOCSharp
{
    public class InputQueue
    {
        public const int INPUT_QUEUE_LENGTH = 128;
        public const int DEFAULT_INPUT_SIZE = 4;

        protected int id;
        protected int head;
        protected int tail;
        protected int length;
        protected bool first_frame;

        protected int last_user_added_frame;
        protected int last_added_frame;
        protected int first_incorrect_frame;
        protected int last_frame_requested;

        protected int frame_delay;

        protected GameInput[] inputs;
        protected GameInput prediction;

        public InputQueue(int inputSize = DEFAULT_INPUT_SIZE)
        {
            Init(-1, inputSize);
        }

        public void Init(int id, int input_size)
        {
            this.id = id;
            this.head = 0;
            this.tail = 0;
            this.length = 0;
            this.frame_delay = 0;
            this.first_frame = true;
            this.last_user_added_frame = GameInput.NULLFRAME;
            this.first_incorrect_frame = GameInput.NULLFRAME;
            this.last_frame_requested = GameInput.NULLFRAME;
            this.last_added_frame = GameInput.NULLFRAME;


            inputs = new GameInput[INPUT_QUEUE_LENGTH];
            this.prediction = new GameInput(GameInput.NULLFRAME, null, input_size);

            for (int i = 0; i < inputs.Length; i++)
            {
                inputs[i] = new GameInput(GameInput.NULLFRAME, null, DEFAULT_INPUT_SIZE);
            }
        }

        public int GetLastConfirmedFrame()
        {
            Debug.Log($"Returning last confirmed frame {last_added_frame}");
            return last_added_frame;
        }

        public int GetFirstIncorrectFrame()
        {
            return first_incorrect_frame;
        }

        public int GetLength()
        {
            return length;
        }

        public void SetFrameDelay(int delay)
        {
            frame_delay = delay;
        }

        public void ResetPrediction(int frame)
        {
            Debug.Assert(first_incorrect_frame == GameInput.NULLFRAME || frame <= first_incorrect_frame);
            Debug.Log($"Resetting all prediction errors back to frame {frame}");

            /*
            * There's nothing really to do other than reset our prediction
            * state and the incorrect frame counter...
            */

            prediction.frame = GameInput.NULLFRAME;
            first_incorrect_frame = GameInput.NULLFRAME;
            last_frame_requested = GameInput.NULLFRAME;
        }

        public void DiscardConfirmedFrames(int frame)
        {
            Debug.Assert(frame >= 0);

            if (last_frame_requested != GameInput.NULLFRAME)
            {
                frame = frame < last_frame_requested ? frame : last_frame_requested;
            }

            Debug.Log($"Discarding confirmed frames up to {frame} (last added:{last_added_frame} " +
                $"length: {length} [head:{head} tail:{tail}");

            if (frame >= last_added_frame)
            {
                tail = head;
            }
            else
            {
                int offset = frame - inputs[tail].frame + 1;
                Debug.Log($"Difference of {offset} frames");

                Debug.Assert(offset >= 0);
                tail = (tail + offset) % INPUT_QUEUE_LENGTH;
                length -= offset;
            }

            Debug.Log($"After discarding new tail is {tail} (frame:{inputs[tail].frame})");
            Debug.Assert(length >= 0);
        }

        public bool GetConfirmedInput(int requested_frame, ref GameInput input)
        {
            Debug.Assert(first_incorrect_frame == GameInput.NULLFRAME || requested_frame < first_incorrect_frame);
            int offset = requested_frame % INPUT_QUEUE_LENGTH;

            if(inputs[offset].frame != requested_frame)
            {
                return false;
            }

            input = inputs[offset];
            return true;
        }

        public bool GetInput(int requested_frame, ref GameInput input)
        {
            Debug.Log($"Requesting input frame {requested_frame}");

            /*
            * No one should ever try to grab any input when we have a prediction
            * error.  Doing so means that we're just going further down the wrong
            * path.  ASSERT this to verify that it's true.
            */

            Debug.Assert(first_incorrect_frame == GameInput.NULLFRAME);

            /*
            * Remember the last requested frame number for later.  We'll need
            * this in AddInput() to drop out of prediction mode.
            */

            last_frame_requested = requested_frame;

            Debug.Assert(requested_frame >= inputs[tail].frame);

            if(prediction.frame == GameInput.NULLFRAME)
            {
                /*
                * If the frame requested is in our range, fetch it out of the queue and
                * return it.
                */

                int offset = requested_frame - inputs[tail].frame;

                if(offset < length)
                {
                    offset = (offset + tail) % INPUT_QUEUE_LENGTH;
                    Debug.Assert(inputs[offset].frame == requested_frame);
                    input = inputs[offset];
                    Debug.Log($"Returning confirmed frame number {input.frame}");
                    return true;
                }

                /*
                * The requested frame isn't in the queue.  Bummer.  This means we need
                * to return a prediction frame.  Predict that the user will do the
                * same thing they did last time.
                */
                if(requested_frame == 0)
                {
                    Debug.Log("Basing new prediction frame from nothing, your client wants frame 0");
                    prediction.Erase();
                }
                else if (last_added_frame == GameInput.NULLFRAME)
                {
                    Debug.Log("Basing new prediction frame from nothing, since we have no frames yet.");
                    prediction.Erase();
                }
                else
                {
                    Debug.Log($"Basing new prediction frame from previously added frame " +
                        $"(queue entry: {PreviousFrame(head)}, frame:{inputs[PreviousFrame(head)].frame}).");
                    prediction = inputs[PreviousFrame(head)];
                }

                prediction.frame++;
            }

            Debug.Assert(prediction.frame >= 0);

            /*
            * If we've made it this far, we must be predicting.  Go ahead and
            * forward the prediction frame contents.  Be sure to return the
            * frame number requested by the client, though.
            */

            input = prediction;
            input.frame = requested_frame;
            Debug.Log($"Returning prediction frame number {input.frame} ({prediction.frame})");

            return false;
        }

        public void AddInput(ref GameInput input)
        {
            int newFrame;

            Debug.Log($"Adding input frame number {input.frame} to queue");

            /*
            * These next two lines simply verify that inputs are passed in 
            * sequentially by the user, regardless of frame delay.
            */

            Debug.Assert(last_user_added_frame == GameInput.NULLFRAME ||
                input.frame == last_user_added_frame + 1);

            last_user_added_frame = input.frame;

            /*
            * Move the queue head to the correct point in preparation to
            * input the frame into the queue.
            */

            newFrame = AdvanceQueueHead(input.frame);

            if(newFrame != GameInput.NULLFRAME)
            {
                AddDelayedInputToQueue(ref input, newFrame);
            }

            /*
            * Update the frame number for the input.  This will also set the
            * frame to GameInput::NullFrame for frames that get dropped (by
            * design).
            */
            input.frame = newFrame;
        }

        protected void AddDelayedInputToQueue(ref GameInput input, int frameNumber)
        {
            Debug.Log($"Adding delayed input frame number {frameNumber} to queue.");

            Debug.Assert(input.size == prediction.size);

            Debug.Assert(last_added_frame == GameInput.NULLFRAME || frameNumber == last_added_frame + 1);

            Debug.Assert(frameNumber == 0 || inputs[PreviousFrame(head)].frame == frameNumber - 1);

            /*
            * Add the frame to the back of the queue
            */

            inputs[head] = input;
            inputs[head].frame = frameNumber;
            head = (head + 1) % INPUT_QUEUE_LENGTH;
            length++;
            first_frame = false;

            last_added_frame = frameNumber;

            if(prediction.frame != GameInput.NULLFRAME)
            {
                Debug.Assert(frameNumber == prediction.frame);

               /*
               * We've been predicting...  See if the inputs we've gotten match
               * what we've been predicting.  If so, don't worry about it.  If not,
               * remember the first input which was incorrect so we can report it
               * in GetFirstIncorrectFrame()
               */

                if(first_incorrect_frame == GameInput.NULLFRAME && prediction.Equal(input, true))
                {
                    Debug.Log($"Frame {frameNumber} does not match prediction. Marking error.");
                    first_incorrect_frame = frameNumber;
                }

               /*
               * If this input is the same frame as the last one requested and we
               * still haven't found any mis-predicted inputs, we can dump out
               * of predition mode entirely!  Otherwise, advance the prediction frame
               * count up.
               */

                if(prediction.frame == last_frame_requested && first_incorrect_frame == GameInput.NULLFRAME)
                {
                    Debug.Log("Prediction is correct! Dumping out of prediction mode.");
                    prediction.frame = GameInput.NULLFRAME;
                }
                else
                {
                    prediction.frame++;
                }
            }

            Debug.Assert(length <= INPUT_QUEUE_LENGTH);
        }

        protected int AdvanceQueueHead(int frame)
        {
            Debug.Log($"Advancing queue head to frame {frame}");

            int expectedFrame = first_frame ? 0 : inputs[PreviousFrame(head)].frame + 1;

            frame += frame_delay;

            if(expectedFrame > frame)
            {
                /*
                * This can occur when the frame delay has dropped since the last
                * time we shoved a frame into the system.  In this case, there's
                * no room on the queue.  Toss it.
                */

                Debug.Log($"Dropping input frame {frame} (expected next frame to be {expectedFrame})");
                return GameInput.NULLFRAME;
            }

            while(expectedFrame < frame)
            {
                /*
                * This can occur when the frame delay has been increased since the last
                * time we shoved a frame into the system.  We need to replicate the
                * last frame in the queue several times in order to fill the space
                * left.
                */

                Debug.Log($"Adding padding frame {expectedFrame} to account for change in frame delay");
                AddDelayedInputToQueue(ref inputs[PreviousFrame(head)], expectedFrame);
                expectedFrame++;
            }

            Debug.Assert(frame == 0 || frame == inputs[PreviousFrame(head)].frame + 1);
            return frame;
        }

        private int PreviousFrame(int offset)
        {
            return (offset == 0) ? (INPUT_QUEUE_LENGTH - 1) : (offset - 1);
        }
    }
}
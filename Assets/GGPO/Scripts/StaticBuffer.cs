﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGPOCSharp
{
    public class StaticBuffer<T>
    {
        protected T[] elements;
        protected int size;

        public StaticBuffer(int N)
        {
            elements = new T[N];
            size = 0;
        }

        public ref T this[int i] 
        {
            get
            {
                Debug.Assert(i >= 0 && i < size);
                return ref elements[i];
            }
        }

        public void PushBack(in T t)
        {
            Debug.Assert(size != (elements.Length - 1));
            elements[size++] = t;
        }

        public int Size()
        {
            return size;
        }
    }
}
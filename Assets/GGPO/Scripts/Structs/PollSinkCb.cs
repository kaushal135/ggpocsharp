﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGPOCSharp
{
    public class PollSinkCb
    {
        public IPollSink sink;
        public object cookie;

        public PollSinkCb()
        {
            sink = null;
            cookie = null;
        }

        public PollSinkCb(IPollSink s)
        {
            sink = s;
        }
    }
}
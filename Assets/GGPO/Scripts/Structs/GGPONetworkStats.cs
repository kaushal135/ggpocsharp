﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGPOCSharp
{
    public struct GGPONetworkStats
    {
        public struct Network
        {
            public int sendQueueLen;
            public int recvieveQueueLen;
            public int ping;
            public int kbpsSent;
        }

        public struct TimeSync
        {
            public int localFramesBehind;
            public int remoteFramesBehind;
        }


        public Network network;
        public TimeSync timeSync;
    }
}
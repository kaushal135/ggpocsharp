﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGPOCSharp
{
    public enum GGPOPlayerType
    {
        GPO_PLAYERTYPE_LOCAL,
        GGPO_PLAYERTYPE_REMOTE,
        GGPO_PLAYERTYPE_SPECTATOR,
    }

    public struct GGPOLocalEndpoint 
    {
        public int playerNum;
    }

    public class GGPOPlayer 
    {
        public int size; //hmm probably not needed but i'll leave it here anyways

        public GGPOPlayerType type;
        public int playerNum;
        public string ipAddress;
        public ushort port;
    }
}
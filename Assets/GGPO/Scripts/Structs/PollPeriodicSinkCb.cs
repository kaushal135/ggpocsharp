﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGPOCSharp
{
    public class PollPeriodicSinkCb : PollSinkCb
    {
        public int interval;
        public int lastFired;

        public PollPeriodicSinkCb()
        {
            sink = null;
            cookie = null;
            interval = 0;
            lastFired = 0;
        }

        public PollPeriodicSinkCb(ref IPollSink s, ref object c, int i)
        {
            sink = s;
            cookie = c;
            interval = i;
            lastFired = 0;
        }
    }
}
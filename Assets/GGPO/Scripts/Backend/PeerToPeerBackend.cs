﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.CompilerServices;
using UnityEngine;
using System.Threading;

namespace GGPOCSharp
{
    public class PeerToPeerBackend : GGPOSession, IUdpCallback
    {
        protected IGGPOSessionCallbacks callbacks;
        protected Poll poll = new Poll();
        protected Sync sync;
        protected Udp udp;
        protected UdpProtocol [] endpoints;
        protected UdpProtocol [] spectators = new UdpProtocol[GGPOConstants.GGPO_MAX_SPECTATORS]; 
        protected int numSpectators = 0;
        protected int inputSize;
        
        protected bool synchronizing = true;
        protected int numPlayers;
        protected int nextRecommendedSleep = 0;
        
        protected int nextSpectatorFrame = 0;
        protected int disconnectTimeout = GGPOConstants.DEFAULT_DISCONNECT_TIMEOUT;
        protected int disconnectNotifyStart = GGPOConstants.DEFAULT_DISCONNECT_NOTIFY_START;
        
        protected ConnectStatus[] localConnectStatus = new ConnectStatus[GGPOConstants.UDP_MSG_MAX_PLAYERS];

        public PeerToPeerBackend(IGGPOSessionCallbacks callbacks, int localPort, int numPlayers, int inputSize)
        {
            this.numPlayers = numPlayers;
            this.inputSize = inputSize;
            this.callbacks = callbacks;

            for (int i = 0; i < GGPOConstants.UDP_MSG_MAX_PLAYERS; i++)
            {
                localConnectStatus[i] = new ConnectStatus();
            }

            for (int i = 0; i < GGPOConstants.GGPO_MAX_SPECTATORS; i++)
            {
                spectators[i] = new UdpProtocol();
            }

            /*
            * Initialize the synchronziation layer
            */
            sync = new Sync(localConnectStatus);
            Sync.Config config = new Sync.Config();
            config.numPlayers = numPlayers;
            config.inputSize = inputSize;
            config.callbacks = callbacks;
            config.numPredictionFrames = GGPOConstants.MAX_PREDICTION_FRAMES;
            sync.Init(ref config);

            /*
            * Initialize the UDP port
            */
            var udpEndpoint = new IPEndPoint(IPAddress.Any, localPort);
            udp = new Udp((ushort)localPort, poll, this);
            endpoints = new UdpProtocol[numPlayers];

            for (int i = 0; i < numPlayers; i++)
            {
                endpoints[i] = new UdpProtocol();
            }

            for (int i = 0; i < localConnectStatus.Length; i++)
            {
                localConnectStatus[i].lastFrame = -1;
            }
        }



        public void OnMsg(IPEndPoint endPoint, NetworkMessage msg)
        {
            for (int i = 0; i < numPlayers; i++)
            {
                if (endpoints[i].HandlesMsg(endPoint, msg))
                {
                    endpoints[i].OnMsg(msg);
                    return;
                }
            }
            for (int i = 0; i < numSpectators; i++)
            {
                if (spectators[i].HandlesMsg(endPoint, msg))
                {
                    spectators[i].OnMsg(msg);
                    return;
                }
            }
        }

        public override GGPOErrorCode AddPlayer(ref GGPOPlayer player, ref int handle)
        {
            if (player.type == GGPOPlayerType.GGPO_PLAYERTYPE_SPECTATOR)
            {
                return AddSpectator(player.ipAddress, player.port);
            }

            int queue = player.playerNum - 1;
            if (player.playerNum < 1 || player.playerNum > numPlayers)
            {
                return GGPOErrorCode.GGPO_ERRORCODE_PLAYER_OUT_OF_RANGE;
            }
            handle = QueueToPlayerHandle(queue);

            if (player.type == GGPOPlayerType.GGPO_PLAYERTYPE_REMOTE)
            {
                AddRemotePlayer(player.ipAddress, player.port, queue);
            }
            return GGPOErrorCode.GGPO_OK;
        }

        public override GGPOErrorCode AddLocalInput(int player, byte[] values)
        {
            int queue = 0;
            GameInput input = new GameInput(GameInput.NULLFRAME, values, values.Length);
            GGPOErrorCode result;

            if (sync.InRollback())
            {
                return GGPOErrorCode.GGPO_ERRORCODE_IN_ROLLBACK;
            }
            if (synchronizing)
            {
                return GGPOErrorCode.GGPO_ERRORCODE_NOT_SYNCHRONIZED;
            }

            result = PlayerHandleToQueue(player, out queue);
            if (result != GGPOErrorCode.GGPO_ERRORCODE_SUCCESS)
            {
                return result;
            }

            // Feed the input for the current frame into the synchronzation layer.
            if (!sync.AddLocalInput(queue, ref input))
            {
                return GGPOErrorCode.GGPO_ERRORCODE_PREDICTION_THRESHOLD;
            }

            if (input.frame != GameInput.NULLFRAME)
            { // xxx: <- comment why this is the case
              // Update the local connect status state to indicate that we've got a
              // confirmed local frame for this player.  this must come first so it
              // gets incorporated into the next packet we send.

                Debug.Log($"setting local connect status for local queue {queue} to {input.frame}");
                localConnectStatus[queue].lastFrame = input.frame;

                // Send the input to all the remote players.
                for (int i = 0; i < numPlayers; i++)
                {
                    if (endpoints[i].IsInitialized())
                    {
                        endpoints[i].SendInput(ref input);
                    }
                }
            }

            return GGPOErrorCode.GGPO_OK;
        }

        public override GGPOErrorCode SyncInput(ref byte[] values, ref int disconnectFlags)
        {
            int flags = 0;

            // Wait until we've started to return inputs.
            if (synchronizing)
            {
                disconnectFlags = 0;
                return GGPOErrorCode.GGPO_ERRORCODE_NOT_SYNCHRONIZED;
            }

            flags = sync.SynchronizeInputs(ref values);

            if (disconnectFlags == 1)
            {
                disconnectFlags = flags;
            }

            return GGPOErrorCode.GGPO_OK;
        }

        public override GGPOErrorCode DoPoll(int timeout)
        {
            if (!sync.InRollback())
            {
                poll.Pump(0);

                PollUdpProtocolEvents();

                if (!synchronizing)
                {
                    sync.CheckSimulation(timeout);

                    // notify all of our endpoints of their local frame number for their
                    // next connection quality report
                    int currentFrame = sync.GetFrameCount();
                    for (int i = 0; i < numPlayers; i++)
                    {
                        endpoints[i].SetLocalFrameNumber(currentFrame);
                    }

                    int totalMinConfirmed;
                    if (numPlayers <= 2)
                    {
                        totalMinConfirmed = Poll2Players(currentFrame);
                    }
                    else
                    {
                        totalMinConfirmed = PollNPlayers(currentFrame);
                    }

                    Debug.Log($"last confirmed frame in p2p backend is {totalMinConfirmed}.");
                    if (totalMinConfirmed >= 0)
                    {
                        Debug.Assert(totalMinConfirmed != int.MaxValue);
                        if (numSpectators > 0)
                        {
                            while (nextSpectatorFrame <= totalMinConfirmed)
                            {
                                Debug.Log($"pushing frame {nextSpectatorFrame} to spectators.");

                                GameInput input = new GameInput();
                                input.frame = nextSpectatorFrame;
                                input.size = inputSize * numPlayers;
                                sync.GetConfirmedInputs(input.bits, nextSpectatorFrame);

                                for (int i = 0; i < numSpectators; i++)
                                {
                                    spectators[i].SendInput(ref input);
                                }
                                nextSpectatorFrame++;
                            }
                        }
                        Debug.Log($"setting confirmed frame in sync to {totalMinConfirmed}.");
                        sync.SetLastConfirmedFrame(totalMinConfirmed);
                    }

                    // send timesync notifications if now is the proper time
                    if (currentFrame > nextRecommendedSleep)
                    {
                        int interval = 0;
                        for (int i = 0; i < numPlayers; i++)
                        {
                            interval = Math.Max(interval, endpoints[i].RecommendFrameDelay());
                        }

                        if (interval > 0)
                        {
                            GGPOEvent info = new GGPOEvent();
                            info.code = GGPOEventCode.GGPO_EVENTCODE_TIMESYNC;
                            info.framesAhead = interval;
                            callbacks.OnEvent(info);
                            nextRecommendedSleep = currentFrame + GGPOConstants.RECOMMENDATION_INTERVAL;
                        }
                    }
                    // XXX: this is obviously a farce...
                    if (timeout == 1)
                    {
                        Thread.Sleep(1);
                    }
                }
            }
            return GGPOErrorCode.GGPO_OK;
        }
        public override GGPOErrorCode IncrementFrame()
        {
            Debug.Log($"End of frame ({sync.GetFrameCount()})...\n");
            sync.IncrementFrame();
            DoPoll(0);
            PollSyncEvents();

            return GGPOErrorCode.GGPO_OK;
        }

        /*
         * Called only as the result of a local decision to disconnect.  The remote
         * decisions to disconnect are a result of us parsing the peer_connect_settings
         * blob in every endpoint periodically.
         */
        public override GGPOErrorCode DisconnectPlayer(int handle)
        {
            int queue = 0;
            GGPOErrorCode result;

            result = PlayerHandleToQueue(handle, out queue);
            if (result != GGPOErrorCode.GGPO_ERRORCODE_SUCCESS)
            {
                return result;
            }

            if (localConnectStatus[queue].disconnected)
            {
                return GGPOErrorCode.GGPO_ERRORCODE_PLAYER_DISCONNECTED;
            }

            if (!endpoints[queue].IsInitialized())
            {
                int currentFrame = sync.GetFrameCount();
                // xxx: we should be tracking who the local player is, but for now assume
                // that if the endpoint is not initalized, this must be the local player.
                Debug.Log($"Disconnecting local player {queue} at frame {localConnectStatus[queue].lastFrame} by user request.");
                for (int i = 0; i < numPlayers; i++)
                {
                    if (endpoints[i].IsInitialized())
                    {
                        DisconnectPlayerQueue(i, currentFrame);
                    }
                }
            }
            else
            {
                Debug.Log($"Disconnecting queue {queue} at frame {localConnectStatus[queue].lastFrame} by user request.");
                DisconnectPlayerQueue(queue, localConnectStatus[queue].lastFrame);
            }
            return GGPOErrorCode.GGPO_OK;
        }

        public override GGPOErrorCode GetNetworkStats(out GGPONetworkStats stats, int handle)
        {
            int queue = 0;
            stats = new GGPONetworkStats();

            GGPOErrorCode result = PlayerHandleToQueue(handle, out queue);
            if (result != GGPOErrorCode.GGPO_ERRORCODE_SUCCESS)
            {
                return result;
            }

            endpoints[queue].GetNetworkStats(out stats);

            return GGPOErrorCode.GGPO_OK;
        }

        public override GGPOErrorCode SetFrameDelay(int player, int delay)
        {
            int queue = 0;
            GGPOErrorCode result = PlayerHandleToQueue(player, out queue);
            
            if (result != GGPOErrorCode.GGPO_ERRORCODE_SUCCESS)
            {
                return result;
            }

            sync.SetFrameDelay(queue, delay);
            return GGPOErrorCode.GGPO_OK;
        }

        public override GGPOErrorCode SetDisconnectTimeout(int timeout)
        {
            disconnectTimeout = timeout;
            for (int i = 0; i < numPlayers; i++)
            {
                if (endpoints[i].IsInitialized())
                {
                    endpoints[i].SetDisconnectTimeout(disconnectTimeout);
                }
            }
            return GGPOErrorCode.GGPO_OK;
        }

        public override GGPOErrorCode SetDisconnectNotifyStart(int timeout)
        {
            disconnectNotifyStart = timeout;
            for (int i = 0; i < numPlayers; i++)
            {
                if (endpoints[i].IsInitialized())
                {
                    endpoints[i].SetDisconnectNotifyStart(disconnectNotifyStart);
                }
            }
            return GGPOErrorCode.GGPO_OK;
        }

        
        protected GGPOErrorCode PlayerHandleToQueue(int player, out int queue)
        {
            int offset = (player - 1);
            queue = -1;

            if (offset < 0 || offset >= numPlayers)
            {
                return GGPOErrorCode.GGPO_ERRORCODE_INVALID_PLAYER_HANDLE;
            }

            queue = offset;
            return GGPOErrorCode.GGPO_OK;
        }
        protected int QueueToPlayerHandle(int queue) 
        { 
            return (queue + 1); 
        }
        protected int QueueToSpectatorHandle(int queue) 
        { 
            return (queue + 1000); /* out of range of the player array, basically */
        } 
        protected void DisconnectPlayerQueue(int queue, int syncTo)
        {
            GGPOEvent info = new GGPOEvent();
            int framecount = sync.GetFrameCount();

            endpoints[queue].Disconnect();

            Debug.Log($"Changing queue {queue} local connect status for last frame " +
                $"from{localConnectStatus[queue].lastFrame} to {syncTo} on disconnect " +
                $"request (current: {framecount}).");

            localConnectStatus[queue].disconnected = true;
            localConnectStatus[queue].lastFrame = syncTo;

            if (syncTo < framecount)
            {
                Debug.Log($"adjusting simulation to account for the fact that {queue} disconnected @ {syncTo}.");
                sync.AdjustSimulation(syncTo);
                Debug.Log("finished adjusting simulation.");
            }
            
            info.code = GGPOEventCode.GGPO_EVENTCODE_DISCONNECTED_FROM_PEER;
            info.playerHandle = QueueToPlayerHandle(queue);
            callbacks.OnEvent(info);

            CheckInitialSync();
        }
        protected void PollSyncEvents()
        {
            Sync.Event e = new Sync.Event();
            while (sync.GetEvent(ref e))
            {
                OnSyncEvent(e);
            }
            return;
        }
        protected void PollUdpProtocolEvents()
        {
            UdpProtocolEvent evt;
            for (int i = 0; i < numPlayers; i++)
            {
                while (endpoints[i].GetEvent(out evt))
                {
                    OnUdpProtocolPeerEvent(evt, i);
                }
            }
            for (int i = 0; i < numSpectators; i++)
            {
                while (spectators[i].GetEvent(out evt))
                {
                    OnUdpProtocolSpectatorEvent(evt, i);
                }
            }
        }
        protected void CheckInitialSync()
        {

            if (synchronizing)
            {
                // Check to see if everyone is now synchronized.  If so,
                // go ahead and tell the client that we're ok to accept input.
                for (int i = 0; i < numPlayers; i++)
                {
                    // xxx: IsInitialized() must go... we're actually using it as a proxy for "represents the local player"
                    if (endpoints[i].IsInitialized() && !endpoints[i].IsSynchronized() && !localConnectStatus[i].disconnected)
                    {
                        return;
                    }
                }
                for (int i = 0; i < numSpectators; i++)
                {
                    if (spectators[i].IsInitialized() && !spectators[i].IsSynchronized())
                    {
                        return;
                    }
                }

                GGPOEvent info = new GGPOEvent();
                info.code = GGPOEventCode.GGPO_EVENTCODE_RUNNING;
                callbacks.OnEvent(info);
                synchronizing = false;
            }
        }
        protected int Poll2Players(int currentFrame)
        {
            int i;

            // discard confirmed frames as appropriate
            int totalMinConfirmed = int.MaxValue;

            for (i = 0; i < numPlayers; i++)
            {
                bool queueConnected = true;
                if (endpoints[i].IsRunning())
                {
                    int ignore;
                    queueConnected = endpoints[i].GetPeerConnectStatus(i, out ignore);
                }
                if (!localConnectStatus[i].disconnected)
                {
                    totalMinConfirmed = Math.Min(localConnectStatus[i].lastFrame, totalMinConfirmed);
                }

                Debug.Log($"local endp: connected = {!localConnectStatus[i].disconnected}, " +
                    $"last_received = {localConnectStatus[i].lastFrame}, " +
                    $"total_min_confirmed = {totalMinConfirmed}." );
                
                if (!queueConnected && !localConnectStatus[i].disconnected)
                {
                    Debug.Log($"disconnecting i {i} by remote request.");
                    DisconnectPlayerQueue(i, totalMinConfirmed);
                }
                Debug.Log($"total_min_confirmed = {totalMinConfirmed}.");
            }
            return totalMinConfirmed;
        }
        protected int PollNPlayers(int currentFrame)
        {
            int lastReceived = 0;

            // discard confirmed frames as appropriate
            int totalMinConfirmed = int.MaxValue;

            for (int queue = 0; queue < numPlayers; queue++)
            {
                bool queueConnected = true;
                int queueMinConfirmed = int.MaxValue;

                Debug.Log($"considering queue {queue}.");

                for (int i = 0; i < numPlayers; i++)
                {
                    // we're going to do a lot of logic here in consideration of endpoint i.
                    // keep accumulating the minimum confirmed point for all n*n packets and
                    // throw away the rest.
                    if (endpoints[i].IsRunning())
                    {
                        bool connected = endpoints[i].GetPeerConnectStatus(queue, out lastReceived);

                        queueConnected = queueConnected && connected;
                        queueMinConfirmed = Math.Min(lastReceived, queueMinConfirmed);

                        Debug.Log($"endpoint {i}: connected = {connected}, " +
                            $"last_received = {lastReceived}, queue_min_confirmed = {queueMinConfirmed}.");
                    }
                    else
                    {
                        Debug.Log($"endpoint {i}: ignoring... not running.");
                    }
                }
                // merge in our local status only if we're still connected!
                if (!localConnectStatus[queue].disconnected)
                {
                    queueMinConfirmed = Math.Min(localConnectStatus[queue].lastFrame, queueMinConfirmed);
                }

                Debug.Log($"local endp: connected = {!localConnectStatus[queue].disconnected}," +
                    $" last_received = {localConnectStatus[queue].lastFrame}, " +
                    $"queue_min_confirmed = {queueMinConfirmed}.");

                if (queueConnected)
                {
                    totalMinConfirmed = Math.Min(queueMinConfirmed, totalMinConfirmed);
                }
                else
                {
                    // check to see if this disconnect notification is further back than we've been before.  If
                    // so, we need to re-adjust.  This can happen when we detect our own disconnect at frame n
                    // and later receive a disconnect notification for frame n-1.
                    if (!localConnectStatus[queue].disconnected || localConnectStatus[queue].lastFrame > queueMinConfirmed)
                    {
                        Debug.Log($"disconnecting queue {queue} by remote request.");
                        DisconnectPlayerQueue(queue, queueMinConfirmed);
                    }
                }
                Debug.Log($"total_min_confirmed = {totalMinConfirmed}.");
            }
            return totalMinConfirmed;
        }
        protected void AddRemotePlayer(string ip, ushort port, int queue)
        {
            /*
            * Start the state machine (xxx: no)
            */
            synchronizing = true;

            endpoints[queue] = new UdpProtocol(udp, poll, queue, ip, port, localConnectStatus);
            endpoints[queue].SetDisconnectTimeout(disconnectTimeout);
            endpoints[queue].SetDisconnectNotifyStart(disconnectNotifyStart);
            endpoints[queue].Synchronize();
        }
        protected GGPOErrorCode AddSpectator(string ip, ushort port)
        {
            if (numSpectators == GGPOConstants.GGPO_MAX_SPECTATORS)
            {
                return GGPOErrorCode.GGPO_ERRORCODE_TOO_MANY_SPECTATORS;
            }
            /*
             * Currently, we can only add spectators before the game starts.
             */
            if (!synchronizing)
            {
                return GGPOErrorCode.GGPO_ERRORCODE_INVALID_REQUEST;
            }
            int queue = numSpectators++;

            spectators[queue] = new UdpProtocol (udp, poll, queue + 1000, ip, port, localConnectStatus);
            spectators[queue].SetDisconnectTimeout(disconnectTimeout);
            spectators[queue].SetDisconnectNotifyStart(disconnectNotifyStart);
            spectators[queue].Synchronize();

            return GGPOErrorCode.GGPO_OK;
        }
        protected virtual void OnSyncEvent(Sync.Event e) 
        {
            
        }
        protected virtual void OnUdpProtocolEvent(UdpProtocolEvent evt, int handle)
        {
            GGPOEvent info = new GGPOEvent();

            switch (evt.EventType)
            {
                case UdpProtocolEvent.EventTypeEnum.Connected:
                    {
                        info.code = GGPOEventCode.GGPO_EVENTCODE_CONNECTED_TO_PEER;
                        info.playerHandle = handle;
                        callbacks.OnEvent(info);
                        break;
                    }
                case UdpProtocolEvent.EventTypeEnum.Synchronizing:
                    {
                        SynchronizingProtocolEvent syncEvent = evt as SynchronizingProtocolEvent;
                        info.code = GGPOEventCode.GGPO_EVENTCODE_SYNCHRONIZING_WITH_PEER;
                        info.playerHandle = handle;
                        info.count = syncEvent.count;
                        info.total = syncEvent.total;
                        callbacks.OnEvent(info);
                        break;
                    }
                case UdpProtocolEvent.EventTypeEnum.Synchronzied:
                    {
                        info.code = GGPOEventCode.GGPO_EVENTCODE_SYNCHRONIZED_WITH_PEER;
                        info.playerHandle = handle;
                        callbacks.OnEvent(info);

                        CheckInitialSync();
                        break;
                    }
                case UdpProtocolEvent.EventTypeEnum.NetworkInterrupted:
                    {
                        NetworkInterruptedProtocolEvent networkInterruptEvt = evt as NetworkInterruptedProtocolEvent;
                        info.code = GGPOEventCode.GGPO_EVENTCODE_CONNECTION_INTERRUPTED;
                        info.playerHandle = handle;
                        info.disconnectTimeout = networkInterruptEvt.disconnectTimeout;
                        callbacks.OnEvent(info);
                        break;
                    }
                case UdpProtocolEvent.EventTypeEnum.NetworkResumed:
                    {
                        info.code = GGPOEventCode.GGPO_EVENTCODE_CONNECTION_RESUMED;
                        info.playerHandle = handle;
                        callbacks.OnEvent(info);
                        break;
                    }
                    
            }
        }
        protected virtual void OnUdpProtocolPeerEvent(UdpProtocolEvent evt, int queue)
        {
            OnUdpProtocolEvent(evt, QueueToPlayerHandle(queue));
            switch (evt.EventType)
            {
                case UdpProtocolEvent.EventTypeEnum.Input:
                    {
                        InputProtocolEvent inputEvt = evt as InputProtocolEvent;
                        if (!localConnectStatus[queue].disconnected)
                        {
                            int currentRemoteFrame = localConnectStatus[queue].lastFrame;
                            int newRemoteFrame = inputEvt.gameInput.frame;
                            Debug.Assert(currentRemoteFrame == -1 || newRemoteFrame == (currentRemoteFrame + 1));

                            GameInput input = inputEvt.gameInput;
                            sync.AddRemoteInput(queue, ref input);
                            // Notify the other endpoints which frame we received from a peer
                            Debug.Log($"setting remote connect status for queue {queue} to {input.frame}");
                            localConnectStatus[queue].lastFrame = input.frame;
                        }
                        break;
                    }
                case UdpProtocolEvent.EventTypeEnum.Disconnected:
                    {
                        DisconnectPlayer(QueueToPlayerHandle(queue));
                        break;
                    }
            }
        }
        protected virtual void OnUdpProtocolSpectatorEvent(UdpProtocolEvent evt, int queue)
        {
            int handle = QueueToSpectatorHandle(queue);
            OnUdpProtocolEvent(evt, handle);

            GGPOEvent info = new GGPOEvent();

            switch (evt.EventType)
            {
                case UdpProtocolEvent.EventTypeEnum.Disconnected:
                    {
                        spectators[queue].Disconnect();

                        info.code = GGPOEventCode.GGPO_EVENTCODE_DISCONNECTED_FROM_PEER;
                        info.playerHandle = handle;
                        callbacks.OnEvent(info);

                        break;
                    }
                    
            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace GGPOCSharp
{
    public class SpectatorBackend : GGPOSession, IUdpCallback
    {
        protected IGGPOSessionCallbacks callbacks;
        protected Poll poll;
        protected Udp udp;
        protected UdpProtocol host;
        protected bool synchronizing;
        protected int inputSize;
        protected int numPlayers;
        protected int nextInputToSend = 0;
        protected GameInput [] inputs = new GameInput[GGPOConstants.SPECTATOR_FRAME_BUFFER_SIZE];

        public SpectatorBackend(IGGPOSessionCallbacks cb, ushort localport, int numPlayers, 
            int inputSize, string hostIP, ushort hostPort)
        {
            this.numPlayers = numPlayers;
            this.inputSize = inputSize;

            callbacks = cb;
            synchronizing = true;

            for (int i = 0; i < inputs.Length; i++)
            {
                inputs[i].frame = GameInput.NULLFRAME;
            }

            /*
            * Initialize the UDP port
            */
            udp = new Udp(localport, poll, this);

            /*
             * Init the host endpoint
             */
            host = new UdpProtocol(udp, poll, 0, hostIP, hostPort, null);
            host.Synchronize();
        }


        public override GGPOErrorCode DoPoll(int timeout)
        {
            poll.Pump(0);

            PollUdpProtocolEvents();
            return GGPOErrorCode.GGPO_OK;
        }
        public override GGPOErrorCode AddPlayer(ref GGPOPlayer player, ref int handle)
        {
            return GGPOErrorCode.GGPO_ERRORCODE_UNSUPPORTED;
        }

        public override GGPOErrorCode AddLocalInput(int player, byte[] values)
        {
            return GGPOErrorCode.GGPO_OK;
        }

        public override GGPOErrorCode SyncInput(ref byte[] values, ref int disconnectFlags)
        {
            // Wait until we've started to return inputs.
            if (synchronizing)
            {
                return GGPOErrorCode.GGPO_ERRORCODE_NOT_SYNCHRONIZED;
            }

            GameInput input = inputs[nextInputToSend % GGPOConstants.SPECTATOR_FRAME_BUFFER_SIZE];
            if (input.frame < nextInputToSend)
            {
                // Haven't received the input from the host yet.  Wait
                return GGPOErrorCode.GGPO_ERRORCODE_PREDICTION_THRESHOLD;
            }
            if (input.frame > nextInputToSend)
            {
                // The host is way way way far ahead of the spectator.  How'd this
                // happen?  Anyway, the input we need is gone forever.
                return GGPOErrorCode.GGPO_ERRORCODE_GENERAL_FAILURE;
            }

            Debug.Assert(values.Length >= inputSize * numPlayers);
            Unsafe.CopyBlock(ref values[0], ref input.bits[0], (uint)(inputSize * numPlayers));

            if (disconnectFlags == 1)
            {
                disconnectFlags = 0; // xxx: should get them from the host!
            }
            nextInputToSend++;

            return GGPOErrorCode.GGPO_OK;
        }

        public override GGPOErrorCode IncrementFrame()
        {
            Debug.Log($"End of frame ({nextInputToSend -1})...");
            DoPoll(0);
            PollUdpProtocolEvents();

            return GGPOErrorCode.GGPO_OK;
        }
        public override GGPOErrorCode DisconnectPlayer(int handle) 
        { 
            return GGPOErrorCode.GGPO_ERRORCODE_UNSUPPORTED; 
        }
        public override GGPOErrorCode GetNetworkStats(out GGPONetworkStats stats, int handle) 
        {
            stats = default;
            return GGPOErrorCode.GGPO_ERRORCODE_UNSUPPORTED; 
        }
        public override GGPOErrorCode SetFrameDelay(int player, int delay) 
        { 
            return GGPOErrorCode.GGPO_ERRORCODE_UNSUPPORTED; 
        }
        public override GGPOErrorCode SetDisconnectTimeout(int timeout) 
        { 
            return GGPOErrorCode.GGPO_ERRORCODE_UNSUPPORTED; 
        }
        public override GGPOErrorCode SetDisconnectNotifyStart(int timeout) 
        { 
            return GGPOErrorCode.GGPO_ERRORCODE_UNSUPPORTED; 
        }

        


        public void OnMsg(IPEndPoint endPoint, NetworkMessage msg)
        {
            if (host.HandlesMsg(endPoint, msg))
            {
                host.OnMsg(msg);
            }
        }


        protected void PollUdpProtocolEvents()
        {
            UdpProtocolEvent evt;
            while (host.GetEvent(out evt))
            {
                OnUdpProtocolEvent(ref evt);
            }
        }


        protected void OnUdpProtocolEvent(ref UdpProtocolEvent evt)
        {
            GGPOEvent info = new GGPOEvent();

            switch (evt.EventType)
            {
                case UdpProtocolEvent.EventTypeEnum.Connected:
                    {
                        info.code = GGPOEventCode. GGPO_EVENTCODE_CONNECTED_TO_PEER;
                        info.playerHandle = 0;
                        callbacks.OnEvent(info);
                        break;
                    }
                case UdpProtocolEvent.EventTypeEnum.Synchronizing:
                    {
                        SynchronizingProtocolEvent syncEvt = evt as SynchronizingProtocolEvent;
                        info.code = GGPOEventCode.GGPO_EVENTCODE_SYNCHRONIZING_WITH_PEER;
                        info.playerHandle = 0;
                        info.count = syncEvt.count;
                        info.total = syncEvt.total;
                        callbacks.OnEvent(info);
                        break;
                    }
                case UdpProtocolEvent.EventTypeEnum.Synchronzied:
                    {
                        if (synchronizing)
                        {
                            info.code = GGPOEventCode.GGPO_EVENTCODE_SYNCHRONIZED_WITH_PEER;
                            info.playerHandle = 0;
                            callbacks.OnEvent(info);

                            info.code = GGPOEventCode.GGPO_EVENTCODE_RUNNING;
                            callbacks.OnEvent(info);
                            synchronizing = false;
                        }
                        break;
                    }
                case UdpProtocolEvent.EventTypeEnum.NetworkInterrupted:
                    {
                        var networkEvt = evt as NetworkInterruptedProtocolEvent;
                        info.code = GGPOEventCode.GGPO_EVENTCODE_CONNECTION_INTERRUPTED;
                        info.playerHandle = 0;
                        info.disconnectTimeout = networkEvt.disconnectTimeout;
                        callbacks.OnEvent(info);
                        break;
                    }
                case UdpProtocolEvent.EventTypeEnum.NetworkResumed:
                    {
                        info.code = GGPOEventCode.GGPO_EVENTCODE_CONNECTION_RESUMED;
                        info.playerHandle = 0;
                        callbacks.OnEvent(info);
                        break;
                    }
                case UdpProtocolEvent.EventTypeEnum.Disconnected:
                    {
                        info.code = GGPOEventCode.GGPO_EVENTCODE_DISCONNECTED_FROM_PEER;
                        info.playerHandle = 0;
                        callbacks.OnEvent(info);
                        break;
                    }
                case UdpProtocolEvent.EventTypeEnum.Input:
                    {
                        var inputEvt = evt as InputProtocolEvent;
                        GameInput input = inputEvt.gameInput;

                        host.SetLocalFrameNumber(input.frame);
                        host.SendInputAck();
                        inputs[input.frame % GGPOConstants. SPECTATOR_FRAME_BUFFER_SIZE] = input;
                        break;
                    }
            }
        }
    }
}
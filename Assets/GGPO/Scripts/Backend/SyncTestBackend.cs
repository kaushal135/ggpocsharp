﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.CompilerServices;

namespace GGPOCSharp
{
    public class SyncTestBackend : GGPOSession
    {
        protected struct SavedInfo
        {
            public int frame;
            public int checksum;
            public int buffer;
            public GameInput input;
        };

        protected IGGPOSessionCallbacks callbacks;
        protected Sync sync;
        protected int numPlayers;
        protected int checkDistance;
        protected int lastVerified;
        protected bool isRollingback;
        protected bool isRunning;

        protected GameInput currentInput;
        protected GameInput lastInput;
        protected RingBuffer<SavedInfo> savedFrames = new RingBuffer<SavedInfo>(32);

        public SyncTestBackend(IGGPOSessionCallbacks cb, int frames, int numPlayers)
        {
            callbacks = cb;
            this.numPlayers = numPlayers;
            checkDistance = frames;
            lastVerified = 0;
            isRollingback = false;
            isRunning = false;
            currentInput.Erase();

            /*
             * Initialize the synchronziation layer
             */
            sync = new Sync(null);
            Sync.Config config = new Sync.Config();
            config.callbacks = callbacks;
            config.numPredictionFrames = GGPOConstants.MAX_PREDICTION_FRAMES;
            sync.Init(ref config);
        }

        public override GGPOErrorCode DoPoll(int timeout)
        {
            if (!isRunning)
            {
                GGPOEvent info = new GGPOEvent();

                info.code = GGPOEventCode.GGPO_EVENTCODE_RUNNING;
                callbacks.OnEvent(info);
                isRunning = true;
            }
            return GGPOErrorCode.GGPO_OK;
        }
        public override GGPOErrorCode AddPlayer(ref GGPOPlayer player, ref int handle)
        {
            if (player.playerNum < 1 || player.playerNum > numPlayers)
            {
                return GGPOErrorCode.GGPO_ERRORCODE_PLAYER_OUT_OF_RANGE;
            }
            handle = (player.playerNum - 1);
            return GGPOErrorCode.GGPO_OK;
        }
        public override GGPOErrorCode AddLocalInput(int player, byte[] values)
        {
            if (!isRunning)
            {
                return GGPOErrorCode.GGPO_ERRORCODE_NOT_SYNCHRONIZED;
            }

            int index = player;

            for (int i = 0; i < values.Length; i++)
            {
                currentInput.bits[(index * values.Length) + i] |= values[i];
            }
            return GGPOErrorCode.GGPO_OK;
        }
        public override GGPOErrorCode SyncInput(ref byte[] values, ref int disconnectFlags)
        {
            if (isRollingback)
            {
                lastInput = savedFrames.Front().input;
            }
            else
            {
                if (sync.GetFrameCount() == 0)
                {
                    sync.SaveCurrentFrame();
                }
                lastInput = currentInput;
            }

            Unsafe.CopyBlock(ref values[0], ref lastInput.bits[0], (uint)values.Length);

            if (disconnectFlags == 1)
            {
                disconnectFlags = 0;
            }
            return GGPOErrorCode.GGPO_OK;
        }
        public override GGPOErrorCode IncrementFrame()
        {
            sync.IncrementFrame();
            currentInput.Erase();

            Debug.Log($"End of frame({sync.GetFrameCount()})...");

            if (isRollingback)
            {
                return GGPOErrorCode.GGPO_OK;
            }

            int frame = sync.GetFrameCount();

            // Hold onto the current frame in our queue of saved states.  We'll need
            // the checksum later to verify that our replay of the same frame got the
            // same results.
            SavedInfo info = new SavedInfo();
            info.frame = frame;
            info.input = lastInput;
            Sync.SavedFrame lastSavedF = sync.GetLastSavedFrame();
            info.buffer = lastSavedF.buffer;
            info.checksum = lastSavedF.checksum;
            savedFrames.Push(info);

            if ((frame - lastVerified) == checkDistance)
            {
                // We've gone far enough ahead and should now start replaying frames.
                // Load the last verified frame and set the rollback flag to true.
                sync.LoadFrame(lastVerified);

                isRollingback = true;
                while (!savedFrames.Empty())
                {
                    callbacks.AdvanceFrame(0);

                    // Verify that the checksumn of this frame is the same as the one in our
                    // list.
                    info = savedFrames.Front();
                    savedFrames.Pop();

                    if (info.frame != sync.GetFrameCount())
                    {
                        RaiseSyncError($"Frame number {info.frame} does not match saved frame number {frame}");
                    }

                    int checksum = sync.GetLastSavedFrame().checksum;

                    if (info.checksum != checksum)
                    {
                        //LogSaveStates(info);
                        RaiseSyncError($"Checksum for frame {frame} does not match saved ({checksum} != {info.checksum})");
                    }
                    Debug.Log($"Checksum {checksum:D8} for frame {info.frame} matches.");
                }
                lastVerified = frame;
                isRollingback = false;
            }

            return GGPOErrorCode.GGPO_OK;
        }

        protected void RaiseSyncError(string ans)
        {
            Debug.LogError(ans);
        }
    }
}
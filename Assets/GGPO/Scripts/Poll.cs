﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using System;

namespace GGPOCSharp
{
    public class Poll 
    {
        public const int MAX_POLLABLE_HANDLES = 64;

        protected int startTime;
        protected int handleCount;
        protected WaitHandle[] handles = new WaitHandle[MAX_POLLABLE_HANDLES];
        protected PollSinkCb[] handleSinks = new PollSinkCb[MAX_POLLABLE_HANDLES];

        protected StaticBuffer<PollSinkCb> msgSinks = new StaticBuffer<PollSinkCb>(16);
        protected StaticBuffer<PollSinkCb> loopSinks = new StaticBuffer<PollSinkCb>(16);
        protected StaticBuffer<PollPeriodicSinkCb> periodicSinks = new StaticBuffer<PollPeriodicSinkCb>(16);

        public Poll()
        {
            handleCount = 0;
            startTime = 0;

            /*
            * Create a dummy handle to simplify things.
            */
            for (int i = 0; i < handles.Length; i++)
            {
                handles[i] = new AutoResetEvent(false);
            }
        }

        public void RegisterHandle(IPollSink sink, ref WaitHandle h, object cookie)
        {
            Debug.Assert(handleCount < MAX_POLLABLE_HANDLES - 1);
            handles[handleCount] = h;
            handleSinks[handleCount] = new PollSinkCb(sink);
            handleCount++;
        }

        public void RegisterMsgLoop(IPollSink sink)
        {
            msgSinks.PushBack(new PollSinkCb(sink));
        }

        public void RegisterLoop(IPollSink sink)
        {
            loopSinks.PushBack(new PollSinkCb(sink));
        }

        public void RegisterPeriodic(ref IPollSink sink, int interval, object cookie)
        {
            periodicSinks.PushBack(new PollPeriodicSinkCb(ref sink, ref cookie, interval));
        }
        
        public void Run()
        {
            while (Pump(100))
            {
                continue;
            }
        }

        public bool Pump(int timeout)
        {
            int i;
            int res;
            bool finished = false;

            if(startTime == 0)
            {
                startTime = (int)Platform.GetCurrentTimeMS();
            }

            int elapsed = (int)Platform.GetCurrentTimeMS() - startTime;
            int maxWait = ComputeWaitTime(elapsed);

            if(maxWait != -1)
            {
                timeout = Math.Min(timeout, maxWait);
            }

            res = WaitHandle.WaitAny(handles, timeout);

            //I dont believe this section is relevant for c#

            //if (res >= WAIT_OBJECT_0 && res < WAIT_OBJECT_0 + _handle_count)
            //{
            //    i = res - WAIT_OBJECT_0;
            //    finished = !_handle_sinks[i].sink->OnHandlePoll(_handle_sinks[i].cookie) || finished;
            //}

            for (i = 0; i < msgSinks.Size(); i++)
            {
                PollSinkCb cb = msgSinks[i];
                finished = !cb.sink.OnMsgPoll(cb.cookie) || finished;
            }

            for (i = 0; i < periodicSinks.Size(); i++)
            {
                PollPeriodicSinkCb cb = periodicSinks[i];
                if (cb.interval + cb.lastFired <= elapsed)
                {
                    cb.lastFired = (elapsed / cb.interval) * cb.interval;
                    finished = !cb.sink.OnPeriodicPoll(cb.cookie, cb.lastFired) || finished;
                }
            }

            for (i = 0; i < loopSinks.Size(); i++)
            {
                PollSinkCb cb = loopSinks[i];
                finished = !cb.sink.OnLoopPoll(cb.cookie) || finished;
            }

            return finished;
        }

        protected int ComputeWaitTime(int elapsed)
        {
            int waitTime = -1;
            int count = periodicSinks.Size();

            if (count > 0)
            {
                for (int i = 0; i < count; i++)
                {
                    PollPeriodicSinkCb cb = periodicSinks[i];
                    int timeout = (cb.interval + cb.lastFired) - elapsed;
                    if (waitTime == -1 || (timeout < waitTime))
                    {
                        waitTime = Math.Max(timeout, 0);
                    }
                }
            }
            return waitTime;
        }
    }
}
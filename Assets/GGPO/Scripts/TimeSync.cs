﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGPOCSharp
{
    public class TimeSync 
    {
        protected int [] local;
        protected int [] remote;
        protected GameInput [] lastInputs;
        protected int nextPrediction;

        static int count = 0;

        public TimeSync()
        {
            local = new int[GGPOConstants.FRAME_WINDOW_SIZE];
            remote = new int[GGPOConstants.FRAME_WINDOW_SIZE];
            lastInputs = new GameInput[GGPOConstants.MIN_UNIQUE_FRAMES];

            for (int i = 0; i < local.Length; i++)
            {
                local[i] = 0;
            }

            for (int i = 0; i < remote.Length; i++)
            {
                remote[i] = 0;
            }

            nextPrediction = GGPOConstants.FRAME_WINDOW_SIZE * 3;
        }

        public void AdvanceFrame(ref GameInput input, int advantage, int radvantage)
        {
            // Remember the last frame and frame advantage
            lastInputs[input.frame % lastInputs.Length] = input;
            local[input.frame % local.Length] = advantage;
            remote[input.frame % remote.Length] = radvantage;
        }

        public int RecommendFrameWaitDuration(bool requireIdleInput)
        {
            // Average our local and remote frame advantages
            int i, sum = 0;
            float advantage, radvantage;

            for (i = 0; i < local.Length; i++)
            {
                sum += local[i];
            }
            advantage = sum / (float)local.Length;

            sum = 0;
            for (i = 0; i < remote.Length; i++)
            {
                sum += remote[i];
            }
            radvantage = sum / (float)remote.Length;

            count = 0;
            count++;

            // See if someone should take action.  The person furthest ahead
            // needs to slow down so the other user can catch up.
            // Only do this if both clients agree on who's ahead!!
            if (advantage >= radvantage)
            {
                return 0;
            }

            // Both clients agree that we're the one ahead.  Split
            // the difference between the two to figure out how long to
            // sleep for.
            int sleepFrames = (int)(((radvantage - advantage) / 2) + 0.5);

            Debug.Log($"iteration {count}:  sleep frames is {sleepFrames}");

            // Some things just aren't worth correcting for.  Make sure
            // the difference is relevant before proceeding.
            if (sleepFrames < GGPOConstants.MIN_FRAME_ADVANTAGE)
            {
                return 0;
            }

            // Make sure our input had been "idle enough" before recommending
            // a sleep.  This tries to make the emulator sleep while the
            // user's input isn't sweeping in arcs (e.g. fireball motions in
            // Street Fighter), which could cause the player to miss moves.
            if (requireIdleInput)
            {
                for (i = 1; i < lastInputs.Length; i++)
                {
                    if (!lastInputs[i].Equal(lastInputs[0], true))
                    {
                        Debug.Log($"iteration {count}:  rejecting due to input stuff at position {i}...!!!");
                        return 0;
                    }
                }
            }

            // Success!!! Recommend the number of frames to sleep and adjust
            return Mathf.Min(sleepFrames, GGPOConstants.MAX_FRAME_ADVANTAGE);
        }
    }
}
﻿using System.Diagnostics;

namespace GGPOCSharp
{
    public class BitVector 
    {
        public const int BITVECTOR_NIBBLE_SIZE = 10;

        public static void BitVectorSetBit(byte[] vector, ref int offset)
        {
            vector[(offset / 8)] |= (byte)(1 << (offset % 8));
            offset += 1;
        }

        public static void BitVectorClearBit(byte[] vector, ref int offset)
        {
            vector[(offset) / 8] &= (byte)~(1 << ((offset) % 8));
            offset += 1;
        }

        public static void BitVectorWriteNibblet(byte[] vector, int nibble, ref int offset)
        {
            Debug.Assert(nibble < (1 << BITVECTOR_NIBBLE_SIZE));

            for (int i = 0; i < BITVECTOR_NIBBLE_SIZE; i++)
            {
                if ((nibble & (1 << i)) != 0)
                {
                    BitVectorSetBit(vector, ref offset);
                }
                else
                {
                    BitVectorClearBit(vector, ref offset);
                }
            }
        }

        public static int BitVectorReadBit(byte[] vector, ref int offset)
        {
            int retval = (vector[(offset) / 8] & (1 << ((offset) % 8))) > 0 ? 1 : 0;
            offset += 1;
            return retval;
        }

        public static int BitVectorReadNibblet(byte[] vector, ref int offset)
        {
            int nibblet = 0;
            for (int i = 0; i < BITVECTOR_NIBBLE_SIZE; i++)
            {
                nibblet |= (BitVectorReadBit(vector, ref offset) << i);
            }
            return nibblet;
        }

    }
}